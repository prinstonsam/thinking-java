package com.prinstonsam.strings.ex11;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by isamsonov on 11/25/15.
 */
public class MyTestRegularEx11 {
    public final static String str = "Arline ate eight apples and one orange while Anita hadn't any";

    public final static String template = "(?i)((^[aeiou])|(\\s+[aeiou]))\\w+?[aeiou]\\b";


    public static void main(String[] args) {
            Pattern pattern = Pattern.compile(template);

            Matcher match = pattern.matcher(str);

            System.out.println("For templates: " + template + " is " + match.find());

    }
}
