package com.prinstonsam.strings.ex07;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 Exercise 7: (5) Using the documentation for java.util.regex.Pattern as
 a resource, write and test a regular expression that checks a sentence to see
 that it begins with a capital letter and ends with a period.
 */
public class MyPrimeRegEx {

    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("^[A-Z].*[.]$");

        Matcher matcher = pattern.matcher("A.");

        if (matcher.matches()) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }
}
