package com.prinstonsam.strings.ex08;

import java.util.Arrays;

/*
Exercise 8: (2) Split the string Splitting.knights on the words “the” or
        “you.”
*/

public class SplittingWithRegEx {
    public static String knights = new StringBuilder("Then, when you have found the shrubbery, you must ")
            .append("cut down the mightiest tree in the forest... ")
            .append("with... a herring!").toString();

    public static void split(String regEx){
        for (String s:knights.split(regEx)){
            System.out.println(s);
        }
    }

    public static void main(String[] args) {
        split("the|you");

    }

}
