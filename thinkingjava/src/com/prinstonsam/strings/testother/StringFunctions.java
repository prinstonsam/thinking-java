package com.prinstonsam.strings.testother;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by isamsonov on 11/12/15.
 */
public class StringFunctions {

    public static void main(String[] args) {

        Date c = new Date();
        String s = String.format("Duke's Birthday: %1$tm %1$te,%1$tY", c);
    }
}
