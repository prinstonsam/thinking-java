package com.prinstonsam.strings.testother;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by isamsonov on 11/16/15.
 */
public class Replacing {
    static String s = Splitting.knights;

    public static void main(String[] args) {
/*
        System.out.println(s);
        System.out.println(s.replaceFirst("f\\w+", "located"));
        System.out.println(s.replaceAll("shrubbery|tree|herring", "banana"));
*/

        System.out.println("ab".matches("[a-z]*ab"));

        Pattern compile = Pattern.compile("[a-z]*ab");

        Matcher matcher = compile.matcher("aab");

        if (matcher.matches()) {
            System.out.println("true");

        } else {
            System.out.println("false");
        }

    }
}
