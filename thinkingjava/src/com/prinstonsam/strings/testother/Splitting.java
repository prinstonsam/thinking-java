package com.prinstonsam.strings.testother;

import java.util.Arrays;

/**
 * Created by isamsonov on 11/16/15.
 */
public class Splitting {
    public static String knights = new StringBuilder("Then, when you have found the shrubbery, you must ")
            .append("cut down the mightiest tree in the forest... ")
            .append("with... a herring!").toString();

    public static void split(String regEx){
        System.out.println(Arrays.toString(knights.split(regEx)));

    }

    public static void main(String[] args) {
        split(" ");
        split(", ");
        split("\\w+");
        split("\\W+");
        split(",\\W+");
    }

}
