package com.prinstonsam.strings.ex02;

import java.util.ArrayList;
import java.util.List;

/**
 Exercise 2: (1) Repair InfiniteRecursion.java.
 */
public class InfiniteRecursion {
    public String toString() {
        return " InfiniteRecursion address: " + super.toString() + "\n";
    }
    public static void main(String[] args) {
        List<InfiniteRecursion> v =
                new ArrayList<>();
        for(int i = 0; i < 10; i++)
            v.add(new InfiniteRecursion());
        System.out.println(v);
    }
}

