package com.prinstonsam.strings.ex09;

/*
Exercise 9: (4) Using the documentation for java.util.regex.Pattern as
a resource, replace all the vowels in Splitting.knights with underscores.
*/

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReplaceVowelsByUnderscore {
    public static String knights = new StringBuilder("Then, when you have found the shrubbery, you must ")
            .append("cut down the mightiest tree in the forest... ")
            .append("with... a herring!").toString();

    public static String replace() {

        Pattern pattern = Pattern.compile("[aeiou]");

        Matcher matcher = pattern.matcher(knights);

        return matcher.replaceAll("_");
    }

    public static void main(String[] args) {
        System.out.println(replace());

        System.out.println("abcabc".matches("(abc)+"));

    }
}
