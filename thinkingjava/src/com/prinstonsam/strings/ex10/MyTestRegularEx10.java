package com.prinstonsam.strings.ex10;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 Exercise 10: (2) For the phrase “Java now has regular expressions”
 evaluate whether the following expressions will find a match:
 ^Java
 \Breg.*
 n.w\s+h(a|i)s
 s?
 s*
 s+
 s{4}
 s{1}.
 s{0,3}

 */
public class MyTestRegularEx10 {

    public final static String str = "Java now has regular expressions";
    public final static String [] templates = {
      "^Java", "|Breg.*", " n.w|s+h(a|i)s", "s?", "s*", "s+", "s{4}", "s{1}", "s{0,3}"
    };


    public static void main(String[] args) {
        for ( String template: templates) {
            Pattern pattern = Pattern.compile( template );

            Matcher match = pattern.matcher(str);

            System.out.println("For templates: " + template + " is " +match.find());

        }
    }

}
