package com.prinstonsam.strings.ex04;//: strings/Receipt.java

/*
Exercise 4: (3) Modify Receipt.java so that the widths are all controlled
        by a single set of constant values. The goal is to allow you to easily change a
        width by changing a single value in one place.
*/

import java.util.*;

public class Receipt {

  private final String WIDTH_FORMAT_FOR_STRING_WITHOUT_NUMBER = "%-15s %5s %10s\n";
  private final String WIDTH_FORMAT_FOR_STRING_WITH_NUMBER = "%-15s %5s %10.2f\n";


  private double total = 0;
  private Formatter f = new Formatter(System.out);
  public void printTitle() {
    f.format(WIDTH_FORMAT_FOR_STRING_WITHOUT_NUMBER, "Item", "Qty", "Price");
    f.format(WIDTH_FORMAT_FOR_STRING_WITHOUT_NUMBER, "----", "---", "-----");
  }
  public void print(String name, int qty, double price) {
    f.format("%-15.15s %5d %10.2f\n", name, qty, price);
    total += price;
  }
  public void printTotal() {
    f.format(WIDTH_FORMAT_FOR_STRING_WITH_NUMBER, "Tax", "", total*0.06);
    f.format(WIDTH_FORMAT_FOR_STRING_WITHOUT_NUMBER, "", "", "-----");
    f.format(WIDTH_FORMAT_FOR_STRING_WITH_NUMBER, "Total", "",
      total * 1.06);
  }
  public static void main(String[] args) {
    Receipt receipt = new Receipt();
    receipt.printTitle();
    receipt.print("Jack's Magic Beans", 4, 4.25);
    receipt.print("Princess Peas", 3, 5.1);
    receipt.print("Three Bears Porridge", 1, 14.29);
    receipt.printTotal();
  }
}