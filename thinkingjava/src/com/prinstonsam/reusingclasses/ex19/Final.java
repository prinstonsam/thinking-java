/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.reusingclasses.ex19;

/**
 *
 * @author isamsonov
 */
class Final {
    public final int final3;

    public final int[] final4;

    public Final(){
        final3 = 10;
        final4 = new int[10];
    }

}

class Main {
    public static void main(String[] args) {

        Final f = new Final();

        int[] array = new int[10];

        f.final4[0] = 10;

    }
}
