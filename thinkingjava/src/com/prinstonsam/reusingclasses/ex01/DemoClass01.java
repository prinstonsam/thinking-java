/*
 * sample of lazy initialization of composition
 */
package com.prinstonsam.reusingclasses.ex01;

/**
 *
 * @author isamsonov
 */
class DemoClass01 {
    CompositionClass composition;

    public void printFromHeaderClass()
    {
        //lazy initialization
        composition = new CompositionClass();

        composition.demoPrint();
    }

    public static void main(String[] args) {

        DemoClass01 headerClass = new DemoClass01();
        headerClass.printFromHeaderClass();
    }

}

class CompositionClass {
    public void demoPrint(){
        System.out.println("Composition Class");
    }
}
