/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.reusingclasses.ex15;

import com.prinstonsam.reusingclasses.ex15.package1.Derived;
import com.prinstonsam.reusingclasses.ex15.package1.Parent;

/**
 *
 * @author isamsonov
 */
public class Other {
    public static void main(String[] args) {
        Parent parent = new Parent();
        Derived derived = new Derived();

        derived.wrapperMethod1();
    }

}
