/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.reusingclasses.ex13;

/**
 *
 * @author isamsonov
 */
public class Main {

    public static void main(String[] args) {
        DerivedClass derived = new DerivedClass();

        derived.method1('a');
        derived.method1(100);
        derived.method1(10000000000l);
        derived.method1("String");


    }

}
