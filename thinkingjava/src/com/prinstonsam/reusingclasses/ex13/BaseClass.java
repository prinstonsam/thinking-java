/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.reusingclasses.ex13;

/**
 *
 * @author isamsonov
 */
public class BaseClass {
    public void method1(int a){
        System.out.println("int");
    }
    public void method1(long a){
        System.out.println("long");
    }
    public void method1(char a){
        System.out.println("char");
    }

}
