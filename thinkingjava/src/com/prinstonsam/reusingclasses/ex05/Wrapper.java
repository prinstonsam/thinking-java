/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.reusingclasses.ex05;

/**
 *
 * @author isamsonov
 */
class A{

    public A() {
        System.out.println("A");
    }


}

class C extends A{
    B b = new B();

}

class B{
    public B() {
        System.out.println("B");
    }

}


class D{

//    public D(String s1, String s2) {
//        System.out.println(s1 + s2);
//    }


}

class E extends D{

//    public E() {
//    }

    public E(String s) {
//        super("D", "E");

        System.out.println(s);
    }





}

public class Wrapper {
    public static void main(String[] args) {
        C c = new C();

        System.out.println(c.toString());

        E e = new E("E");

        System.out.println(e.toString());
    }

}
