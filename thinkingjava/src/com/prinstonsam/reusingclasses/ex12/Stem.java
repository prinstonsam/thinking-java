/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.reusingclasses.ex12;

/**
 *
 * @author isamsonov
 */
public class Stem extends Root{
    Component1 component1;
    Component2 component2;
    Component3 component3;

    public Stem() {
        component1 = new Component1();
        component2 = new Component2();
        component3 = new Component3();
    }

    @Override
    public void dispose(){
        System.out.println("Stem dispose");
        component1.dispose();
        component2.dispose();
        component3.dispose();
        super.dispose();
    }
}
