/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.reusingclasses.ex07;

/**
 *
 * @author isamsonov
 */
class A{
    public A(String s) {
        System.out.println(s);
    }
}

class C extends A{

    public C(String s) {
        super(s);

    }

    B b = new B("B");
}

class B{
    public B(String s) {
        System.out.println(s);
    }
}

public class Wrapper {
    public static void main(String[] args) {
        C c = new C("A");

        System.out.println(c.toString());

    }

}
