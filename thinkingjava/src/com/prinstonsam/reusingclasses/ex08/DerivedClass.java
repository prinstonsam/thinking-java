/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.reusingclasses.ex08;

/**
 *
 * @author isamsonov
 */
public class DerivedClass extends BaseClass{

    public DerivedClass() {
        super("Base class");
    }

    public DerivedClass(String value) {
        super("Base class");
        System.out.println(value);
    }



}
