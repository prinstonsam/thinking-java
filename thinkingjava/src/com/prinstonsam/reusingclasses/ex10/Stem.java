/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.reusingclasses.ex10;



/**
 *
 * @author isamsonov
 */
public class Stem extends Root{
    Component1 component1;
    Component2 component2;
    Component3 component3;

    public Stem(String value) {
        super("Root");
        component1 = new Component1("Component1");
        component2 = new Component2("Component2");
        component3 = new Component3("Component3");
        System.out.println(value);
    }
}
