package com.prinstonsam.errorhandling.ex27;

/*
Exercise 27: (1) Modify Exercise 3 to convert the exception to a
RuntimeException.
*/

public class Main {
    public static void main(String[] args) {
        try {
            int [] arr = new int[10];
            for (int i = 0; i < arr.length + 1; i++) {
                System.out.println(arr[i]);
            }
        } catch (RuntimeException ex) {
            System.out.println("It is");
            ex.printStackTrace();
        }finally {
            System.out.println("i was here");
        }
    }
}
