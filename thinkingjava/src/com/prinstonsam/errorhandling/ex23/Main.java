package com.prinstonsam.errorhandling.ex23;

import java.io.IOException;

/**
 * Created by isamsonov on 11/5/15.
 */
public class Main {
    public static void main(String[] args) {
        //FailingConstructor failingConstructor = null;
        try {
            FailingConstructor failingConstructor = new FailingConstructor();
            try {
                System.out.println("Processing");
            }
            finally {
                //we don't have methods for cleanup
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
