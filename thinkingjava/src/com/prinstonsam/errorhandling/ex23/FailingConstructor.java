package com.prinstonsam.errorhandling.ex23;

import java.io.IOException;

/**
 Exercise 23: (4) Add a class with a dispose( ) method to the previous
 exercise. Modify FailingConstructor so that the constructor creates one of
 these disposable objects as a member object, after which the constructor
 might throw an exception, after which it creates a second disposable member
 object. Write code to properly guard against failure, and in main( ) verify
 that all possible failure situations are covered.
 */

class Disposable{
    public void dispose(){
        System.out.println("Disposable " + toString());
    }
}
public class FailingConstructor {

    private Disposable dis1;
    private Disposable dis2;


    public FailingConstructor() throws IOException {
        dis1 = new Disposable();
        if(dis2 == null) {
            throw new IOException();
        }
        dis2 = new Disposable();
    }

    public Disposable getDis2() {
        return dis2;
    }

    public void setDis2(Disposable dis2) {
        this.dis2 = dis2;
    }

    public Disposable getDis1() {
        return dis1;
    }

    public void setDis1(Disposable dis1) {
        this.dis1 = dis1;
    }
}
