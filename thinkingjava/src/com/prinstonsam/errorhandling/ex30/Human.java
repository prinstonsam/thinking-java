package com.prinstonsam.errorhandling.ex30;
/*
Exercise 30: (2) Modify Human.java so that the exceptions inherit
        from RuntimeException. Modify main( ) so that the technique in
        TurnOffChecking.java is used to handle the different types of exceptions.
*/


class Annoyance extends RuntimeException {
}

class Sneeze extends Exception {
}

public class Human {
    public static void main(String[] args) {
        // Catch the exact type:
        try {
            throw new Sneeze();
        } catch (Sneeze s) {
            System.out.println("Caught RuntimeException");
        } catch (Annoyance a) {
            System.out.println("Caught Annoyance");
        }
        // Catch the base type:
        try {
            throw new Sneeze();
        } catch (Exception a) {
            System.out.println("Caught Exception");
        }
    }
} /* Output:
Caught Sneeze
Caught Annoyance
*///:~
