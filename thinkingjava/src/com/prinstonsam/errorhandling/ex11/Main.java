package com.prinstonsam.errorhandling.ex11;

/**
 * Exercise 10: (2) Create a class with two methods, f( ) and g( ). In g( ),
 * throw an exception of a new type that you define. In f( ), call g( ), catch its
 * exception and, in the catch clause, throw a different exception (of a second
 * type that you define). Test your code in main( ).
 */

class MyException01 extends Exception{
    @Override
    public String getMessage() {
        return "MyExcption01";
    }
}

class MyException02 extends Exception{
    @Override
    public String getMessage() {
        return "MyExcption02";
    }
}

class MyClass {
    public void f() {

        try {
            g();
        } catch (MyException01 ex) {
            ex.getMessage();
            try {
                throw new MyException02();
            } catch (MyException02 myException02) {
                myException02.printStackTrace();
            }
        }


    }
    public void g() throws MyException01 {
        throw new MyException01();
    }
}

public class Main {
    public static void main(String[] args) {
        MyClass myClass = new MyClass();

        myClass.f();
    }
}
