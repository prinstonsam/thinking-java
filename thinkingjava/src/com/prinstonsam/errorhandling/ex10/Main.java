package com.prinstonsam.errorhandling.ex10;

/**
 Exercise 11: (1) Repeat the previous exercise, but inside the catch
 clause, wrap g( )’s exception in a RuntimeException.

 */

class MyException01 extends Exception{
    @Override
    public String getMessage() {
        return "MyExcption01";
    }
}

class MyException02 extends Exception{
    @Override
    public String getMessage() {
        return "MyExcption02";
    }
}

class MyClass {
    public void f() {

        try {
            g();
        } catch (MyException01 ex) {
            ex.getMessage();
            try {
                throw new RuntimeException();
            } catch (RuntimeException myException02) {
                myException02.printStackTrace();
            }
        }


    }
    public void g() throws MyException01 {
        throw new MyException01();
    }
}

public class Main {
    public static void main(String[] args) {
        MyClass myClass = new MyClass();

        myClass.f();
    }
}
