package com.prinstonsam.errorhandling.ex22;

import java.io.IOException;

/**
 * Created by isamsonov on 11/5/15.
 */
public class Main {
    public static void main(String[] args) {
        //FailingConstructor failingConstructor = null;
        try {
            FailingConstructor failingConstructor = new FailingConstructor();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
