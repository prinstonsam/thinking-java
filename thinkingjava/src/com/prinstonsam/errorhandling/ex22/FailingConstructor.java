package com.prinstonsam.errorhandling.ex22;

import java.io.IOException;

/**
 Exercise 22: (2) Create a class called FailingConstructor with a
 constructor that might fail partway through the construction process and
 throw an exception. In main( ), write code that properly guards against this
 failure.

 */
public class FailingConstructor {

    public FailingConstructor() throws IOException {
        System.out.println("before fail");
        throw new IOException();
    }
}
