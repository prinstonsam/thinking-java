package com.prinstonsam.errorhandling.fromwork;

/**
 * Created by isamsonov on 12/9/15.
 */
class My2{
    public void method02() throws Exception{
        throw new Exception();
    }
}

class My1{
    private final static My2 endUser = new My2();

    static
    {
        try
        {
            endUser.method02();
        }
        catch (Exception ex)
        {
            throw new RuntimeException(ex);
        }
    }

    public static int method01() throws Exception{
        return 1;
    }
}


public class MyPrimer {
    public static void main(String[] args) {
        try {
            System.out.println(My1.method01());
        } catch (RuntimeException e1){
            System.out.println("Ura1");
        } catch (Exception e) {
            System.out.println("Ura");;
        }
        catch (ExceptionInInitializerError e)
        {
            System.out.println("ohoooo");
        }
    }

}
