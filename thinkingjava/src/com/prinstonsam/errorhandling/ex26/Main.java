package com.prinstonsam.errorhandling.ex26;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by isamsonov on 11/10/15.
 */
public class Main {
    public static void main(String[] args) throws IOException {
        FileInputStream aaa = new FileInputStream("aaa");

        aaa.close();
    }
}
