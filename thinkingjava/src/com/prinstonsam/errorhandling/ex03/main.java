package com.prinstonsam.errorhandling.ex03;

/*
Exercise 3: (1) Write code to generate and catch an
        ArrayIndexOutOfBoundsException.
*/

public class main {
    public static void main(String[] args) {
        try {
            int [] arr = new int[10];
            for (int i = 0; i < arr.length + 1; i++) {
                System.out.println(arr[i]);
            }
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("It is");
            ex.printStackTrace();
        }finally {
            System.out.println("i was here");
        }
    }
}
