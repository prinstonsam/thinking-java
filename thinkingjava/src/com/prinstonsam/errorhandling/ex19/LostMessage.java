package com.prinstonsam.errorhandling.ex19;

/*
Exercise 19: (2) Repair the problem in LostMessage.java by guarding
the call in the finally clause.

*/

class VeryImportantException extends Exception {
    public String toString() {
        return "A very important exception!";
    }
}

class HoHumException01 extends Exception {
    public String toString() {
        return "A trivial exception";
    }
}

class HoHumException02 extends Exception {
    public String toString() {
        return "A second level trivial exception";
    }
}

public class LostMessage {
    void f() throws VeryImportantException {
        throw new VeryImportantException();
    }
    void dispose() throws HoHumException01 {
        throw new HoHumException01();
    }

    void disposeSecondLevel() throws HoHumException02 {
        throw new HoHumException02();
    }
    public static void main(String[] args) {
        try {
            LostMessage lm = new LostMessage();
            try {
                lm.f();
            } finally {
                try {
                    lm.dispose();
                }
                catch (HoHumException01 ex1){
                    System.out.println(ex1.getMessage());
                } finally {
                    try {
                        lm.disposeSecondLevel();
                    } catch (HoHumException02 ex2) {
                        System.out.println(ex2.getMessage());
                    }
                }
            }
        } catch(Exception e) {
            System.out.println(e);
        }
    }
}
