package com.prinstonsam.errorhandling.ex05;

/*
Exercise 5: (3) Create your own resumption-like behavior using a while
        loop that repeats until an exception is no longer thrown.
*/
import org.apache.log4j.*;

public class main {
    static Logger logger = Logger.getLogger(main.class);

    public static void main(String[] args) {
//        BasicConfigurator.configure();


/*
        int i=100;
        while (i>10) {
            try {
                i--;
                throw new Exception("aa");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
*/
        logger.setLevel(Level.DEBUG);
        logger.info("aaa");

        System.out.println(logger);
    }
}
