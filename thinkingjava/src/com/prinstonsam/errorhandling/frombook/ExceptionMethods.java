package com.prinstonsam.errorhandling.frombook;

/**
 * Created by isamsonov on 11/3/15.
 */

import static net.mindview.util.Print.*;

public class ExceptionMethods {
    public static void main(String[] args) {
        try {
            throw new Exception("My Exception");
        } catch(Exception e) {
            print("Caught Exception");
            print("-------------------");
            print("getMessage():" + e.getMessage());
            print("-------------------");
            print("getLocalizedMessage():" +
                    e.getLocalizedMessage());
            print("-------------------");
            print("toString():" + e);
            print("-------------------");
            print("printStackTrace():");
            print("-------------------");
            e.printStackTrace(System.out);
        }
    }
}
