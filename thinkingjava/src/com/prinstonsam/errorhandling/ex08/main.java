package com.prinstonsam.errorhandling.ex08;

/*
Exercise 8: (1) Write a class with a method that throws an exception of
the type created in Exercise 4. Try compiling it without an exception
specification to see what the compiler says. Add the appropriate exception
specification. Try out your class and its exception inside a try-catch clause.

*/
class MyException extends Exception{
    private String stringException;
    public MyException(String stringException) {
        this.stringException = stringException;
    }

    public void printMyMessage(){
        System.out.println(stringException);
    }
}

class MyTestException{
    public void method01() throws MyException {
        throw new MyException("My message about exception");
    }
}

public class main {
    public static void main(String[] args) {
        MyTestException testException = new MyTestException();
        try {
            testException.method01();
        } catch (MyException ex) {
            ex.printMyMessage();
            ex.printStackTrace();
        }
    }
}
