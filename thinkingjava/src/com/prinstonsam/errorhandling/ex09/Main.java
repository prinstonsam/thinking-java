package com.prinstonsam.errorhandling.ex09;

import java.io.IOException;

/**
 Exercise 9: (2) Create three new types of exceptions. Write a class with a
 method that throws all three. In main( ), call the method but only use a
 single catch clause that will catch all three types of exceptions

 */
class MyException01 extends Exception{
    public MyException01() {
    }

    public MyException01(String message) {
        super(message);
    }
}

class MyException02 extends Exception{
    public MyException02() {
    }

    public MyException02(String message) {
        super(message);
    }
}

class MyException03 extends Exception{
    public MyException03() {
    }

    public MyException03(String message) {
        super(message);
    }
}

class MyThrownAllException {
    public void method01() throws MyException01 {
        throw new MyException01("MyException01");
        //we aren't forced to throw all the exception in the specification
    }
}

public class Main {

    public static void main(String[] args) {
        MyThrownAllException thrownAllException = new MyThrownAllException();

        try {
            thrownAllException.method01();

        } catch (MyException01 ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }
}
