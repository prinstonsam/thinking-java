package com.prinstonsam.errorhandling.ex18;

/*
Exercise 18: (3) Add a second level of exception loss to
        LostMessage.java so that the HoHumException is itself replaced by a
        third exception.
*/

class VeryImportantException extends Exception {
    public String toString() {
        return "A very important exception!";
    }
}
class HoHumException01 extends Exception {
    public String toString() {
        return "A trivial exception";
    }
}

class HoHumException02 extends Exception {
    public String toString() {
        return "A second level trivial exception";
    }
}

public class LostMessage {
    void f() throws VeryImportantException {
        throw new VeryImportantException();
    }
    void dispose() throws HoHumException01 {
        throw new HoHumException01();
    }

    void disposeSecondLevel() throws  HoHumException02 {
        throw new HoHumException02();
    }
    public static void main(String[] args) {
        try {
            LostMessage lm = new LostMessage();
            try {
                lm.f();
            } finally {
                try{
                    lm.dispose();
                    }finally {
                    lm.disposeSecondLevel();
                }
            }
        } catch(Exception e) {
            System.out.println(e);
        }
    }
}
