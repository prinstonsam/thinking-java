package com.prinstonsam.errorhandling.ex21;

/**
 Exercise 21: (2) Demonstrate that a derived-class constructor cannot
 catch exceptions thrown by its base-class constructor.

 */

class Base{
    public Base(String s) throws Exception {
        throw new Exception();
    }
}

class Derived extends Base{


    public Derived() throws Exception {
        super("aaa");
        //here we can't try, because super should to be first

    }
}

public class Main {
    public static void main(String[] args) {
        try {
            Derived derived = new Derived();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
