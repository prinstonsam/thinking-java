/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.errorhandling.ex12;

/**
 *
 Exercise 12: (3) Modify innerclasses/Sequence.java so that it throws
 an appropriate exception if you try to put in too many elements.

 */
public class Main {
    public static void main(String[] args) {
        Sequence sequence = new Sequence(10);

        for (int i = 0; i < 10; i++) {
            sequence.add(new MyString("String number:" +i));
        }

        Selector selector = sequence.selector();

        while( !selector.end()){
            System.out.println(selector.current().toString());
            selector.next();
        }
    }

}
