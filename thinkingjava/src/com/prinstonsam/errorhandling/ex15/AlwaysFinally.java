package com.prinstonsam.errorhandling.ex15;

/**
 * Created by isamsonov on 11/5/15.
 */
import static net.mindview.util.Print.*;
class FourException extends Exception {}
class MyRuntimeException extends RuntimeException{}

public class AlwaysFinally {
    public static void main(String[] args) {
        print("Entering first try block");
        try {
            print("Entering second try block");
            try {
                throw new MyRuntimeException();
            } finally {
                print("finally in 2nd try block");
            }
        } catch(MyRuntimeException e) {
            System.out.println(
                    "Caught runtime in 1st try block");
/*
        } catch(FourException e) {
            System.out.println(
                    "Caught FourException in 1st try block");
*/
        } finally {
            System.out.println("finally in 1st try block");
        }
    }
}

