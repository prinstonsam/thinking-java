package com.prinstonsam.errorhandling.ex02;

/*
Exercise 2: (1) Define an object reference and initialize it to null. Try to
        call a method through this reference. Now wrap the code in a try-catch
        clause to catch the exception.
*/

public class main {
    public static void main(String[] args) {
        Object obj = null;

        try {
            obj.toString();

        } catch (NullPointerException npe) {
            System.out.println("You are idiot");
            npe.printStackTrace();
        }finally {
            System.out.println("i was here");
        }
    }
}
