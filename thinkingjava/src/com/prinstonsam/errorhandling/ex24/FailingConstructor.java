package com.prinstonsam.errorhandling.ex24;

import java.io.IOException;

/**
 Exercise 22: (2) Create a class called FailingConstructor with a
 constructor that might fail partway through the construction process and
 throw an exception. In main( ), write code that properly guards against this
 failure.

 */
public class FailingConstructor {

    public FailingConstructor() {
        System.out.println("before fail");
        try {
            throw new IOException();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            dispose();
        }
    }

    public void dispose(){
        System.out.println("Dispose");
    }
}
