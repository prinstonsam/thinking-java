/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.polymorphism.ex14;

/**
 *
 * @author isamsonov
 */
public class Hamster extends Rodent{

    private static long counter = 0;
    private final long id = counter++;


    public Hamster() {
        size = Size.BIG;
        System.out.println(this);
    }

    @Override
    public String toString() {
        return "Harmster " + id;
    }

    @Override
    public void dispose() {
        counter--;
        System.out.println("Disposing " + this);

    }





}
