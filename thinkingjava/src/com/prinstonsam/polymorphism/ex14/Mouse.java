/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.polymorphism.ex14;


/**
 *
 * @author isamsonov
 */
public class Mouse extends Rodent{
    private static long counter = 0;
    private final long id = counter++;

    public Mouse() {
        System.out.println(this);
        size = Size.SMALL;
    }

    @Override
    public String toString() {
        return "Mouse " + id;
    }

    @Override
    public void dispose() {
        counter--;
        System.out.println("Disposing " + this);

    }





}
