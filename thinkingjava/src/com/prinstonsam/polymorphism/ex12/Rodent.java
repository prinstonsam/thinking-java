/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.polymorphism.ex12;

/**
 *
 * @author isamsonov
 */
public class Rodent {

    protected Size size;

    public Rodent() {
        System.out.println("Rodent");
        size = Size.NaN;
    }



    public Size getSize(){
        return size;
    }

}
