/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.polymorphism.ex12;

import java.util.Random;

/**
 *
 * @author isamsonov
 */
public class RandomRodentGenerator {
    Random rand = new Random(41);

    public Rodent next() {
        switch (rand.nextInt(4)){
            default:
            case 0:
                return new Mouse();
            case 1:
                return new Gerbil();
            case 2:
                return new Hamster();
        }
    }

}
