//: polymorphism/music/Note.java
// Notes to play on musical instruments.
package com.prinstonsam.polymorphism;

public enum Note {
    MIDDLE_C, C_SHARP, B_FLAT; // Etc.
} ///:~
