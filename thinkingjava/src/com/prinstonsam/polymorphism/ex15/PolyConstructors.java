/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.polymorphism.ex15;

/**
 *
 * @author isamsonov
 */
class Glyph {
    void draw() {
        System.out.println("Glyph.draw()");
    }
    Glyph() {
        System.out.println("Glyph() before draw()");
        draw();
        System.out.println("Glyph() after draw()");
    }
}

class RoundGlyph extends Glyph {
    private int radius = 1;
    RoundGlyph( int r ) {
        radius = r;
        System.out.println("Round.Glyph.RoundGlyph() , radius  = " + radius);
    }
    void draw() {
        System.out.println( "RoundGlyph.draw(), radius = " + radius );
    }
}

public class PolyConstructors {
    public static void main(String[] args) {
        new RoundGlyph(5);
    }


}
