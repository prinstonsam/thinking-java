/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.polymorphism.ex09;

/**
 *
 * @author isamsonov
 */
public class Main {

    public static void printAll(Rodent[] rodents){

        for (Rodent rodent : rodents) {
            System.out.println(rodent.getSize());

        }

    }

    public static void main(String[] args) {
        Rodent[] rodents = new Rodent[10];
        RandomRodentGenerator fabric = new RandomRodentGenerator();

        for (int i = 0; i < rodents.length; i++) {
            rodents[i] = fabric.next();

        }

        printAll(rodents);
    }

}
