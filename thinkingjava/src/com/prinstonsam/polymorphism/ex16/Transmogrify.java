package com.prinstonsam.polymorphism.ex16;

class Actor {

    public void act() {
    }
}

class HappyActor extends Actor {

    public void act() {
        System.out.println("HappyActor");
    }
}

class SadActor extends Actor {

    public void act() {
        System.out.println("SadActor");
    }
}

class Stage {

    private Actor actor = new HappyActor();

    public void change() {
        actor = new SadActor();
    }

    public void performPlay() {
        actor.act();
    }
}

class Starship{
    AlertStatus status = AlertStatus.FIRST;

    public void changeStatus(AlertStatus status){
        this.status = status;
    }

    public AlertStatus getStatus() {
        return status;
    }



}

enum AlertStatus {
    FIRST, SECOND, THIRD;
}


public class Transmogrify {

    public static void main(String[] args) {
        Stage stage = new Stage();
        stage.performPlay();
        stage.change();
        stage.performPlay();

        Starship s = new Starship();

        s.changeStatus(AlertStatus.THIRD);
        System.out.println(s.getStatus());
    }
}
