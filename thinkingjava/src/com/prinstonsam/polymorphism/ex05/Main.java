/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.polymorphism.ex05;

/**
 *
 * @author isamsonov
 */
public class Main {
    public static void test( Cycle cycle ){
        cycle.ride();
        cycle.wheels();
    }

    public static void main(String[] args) {
        Bicycle bicycle = new Bicycle();
        Tricycle tricycle = new Tricycle();
        Unicycle unicycle = new Unicycle();

        test(bicycle);
        test(tricycle);
        test(unicycle);

    }
}
