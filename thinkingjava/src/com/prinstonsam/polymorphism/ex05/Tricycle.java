package com.prinstonsam.polymorphism.ex05;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author isamsonov
 */
public class Tricycle extends Cycle{
    @Override
    public void ride( ) {
        System.out.println("Tricycle");
    }

    @Override
    public void wheels() {
        System.out.println("Three wheels");
    }


}
