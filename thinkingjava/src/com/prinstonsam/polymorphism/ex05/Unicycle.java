/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.polymorphism.ex05;

/**
 *
 * @author isamsonov
 */
public class Unicycle extends Cycle {
    @Override
    public void ride( ) {
        System.out.println("Unicycle");
    }

    @Override
    public void wheels() {
        System.out.println("One wheel");
    }
}
