/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.polymorphism.ex11;

class Meal {
    public Meal() {
        System.out.println("Meal()");
    }
}

class Bread{
    public Bread() {
        System.out.println("Bread()");
    }
}

class Cheese{
    public Cheese() {
        System.out.println("Cheese");
    }
}

class Lettuce{
    public Lettuce() {
        System.out.println("Lettuce");
    }
}

class Pickle{

    public Pickle() {
        System.out.println("Pickle()");
    }

}

class Lunch extends Meal {

    public Lunch() {
        System.out.println("Lunch()");
    }

}

class PortableLunch extends Lunch {

    public PortableLunch() {
        System.out.println("PortableLunch()");
    }

}
public class Sandwich extends PortableLunch{
    private Bread b = new Bread();
    private Cheese c = new Cheese();
    private Lettuce l = new Lettuce();
    private Pickle p = new Pickle();

    public Sandwich() {
        System.out.println("Sandwich");
    }

    public static void main(String[] args) {
        new Sandwich();


    }




}
