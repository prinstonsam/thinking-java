/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.polymorphism.ex06;

import com.prinstonsam.polymorphism.Note;

/**
 *
 * @author isamsonov
 */
public class Wind extends Instrument {


    @Override
    void play(Note n) {
        System.out.println("Instrument.play() " + n);
    }

    @Override
    public String what() {
        return toString();
    }

    @Override
    public void adjust() {
    }
}
