/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.polymorphism.ex08;

import java.util.Random;

/**
 *
 * @author isamsonov
 */
public class RandomInstrumentGenerator {

    private final Random rand = new Random(47);

    public Instrument next() {
        switch (rand.nextInt(7)) {
            default:
            case 0:
                return new Wind();
            case 1:
                return new Percussion();
            case 2:
                return new Stringed();
            case 3:
                return new Brass();
            case 4:
                return new Woodwind();
            case 5:
                return new Brum();
        }
    }

}
