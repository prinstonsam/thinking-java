/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.polymorphism.ex08;
import com.prinstonsam.polymorphism.Note;

/**
 *
 * @author isamsonov
 */
public class Music3 {

    static void tune(Instrument i) {
// ...
        i.play(Note.MIDDLE_C);
    }

    static void tuneAll(Instrument[] e) {
        for (Instrument i : e) {
            tune(i);
            System.out.println(i.what());
        }
    }
    public static void main(String[] args) {
// Upcasting during addition to the array:

        RandomInstrumentGenerator gen = new RandomInstrumentGenerator();

        Instrument[] orchestra = new Instrument[25];

        for (int i = 0; i < orchestra.length; i++) {
            orchestra[i] = gen.next();
        }

        tuneAll(orchestra);
    }
}


