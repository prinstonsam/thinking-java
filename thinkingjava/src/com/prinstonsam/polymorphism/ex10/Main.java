/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.polymorphism.ex10;

/**
 *
 * @author isamsonov
 */
public class Main {


    public static void print(Base b){
        b.first();
    }

    public static void main(String[] args) {
        Derived d = new Derived();
        print(d);


    }

}
