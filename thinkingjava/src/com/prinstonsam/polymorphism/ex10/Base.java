/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.polymorphism.ex10;

/**
 *
 * @author isamsonov
 */
public class Base {
    public void first(){
        second();
    }

    public void second(){
        System.out.println("Base.second()");
    }

}
