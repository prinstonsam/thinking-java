/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.polymorphism.ex07;
import com.prinstonsam.polymorphism.Note;
/**
 *
 * @author isamsonov
 */
public class Music3 {
// Doesn't care about type, so new types
// added to the system still work right:

    static void tune(Instrument i) {
// ...
        i.play(Note.MIDDLE_C);
    }

    static void tuneAll(Instrument[] e) {
        for (Instrument i : e) {
            tune(i);
            System.out.println(i.what());
        }
    }

    public static void main(String[] args) {
// Upcasting during addition to the array:
        Instrument[] orchestra = {
            new Wind(),
            new Percussion(),
            new Stringed(),
            new Brass(),
            new Woodwind(),
            new Brum()
        };
        tuneAll(orchestra);
    }
}

