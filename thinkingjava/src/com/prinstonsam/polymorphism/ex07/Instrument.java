/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.polymorphism.ex07;

import com.prinstonsam.polymorphism.Note;

/**
 *
 * @author isamsonov
 */
public class Instrument {

    void play(Note n) {
        System.out.println("Instrument.play() " + n);
    }

    String what() {
        return toString();
    }

    void adjust() {
        System.out.println("Adjusting Instrument");
    }
}
