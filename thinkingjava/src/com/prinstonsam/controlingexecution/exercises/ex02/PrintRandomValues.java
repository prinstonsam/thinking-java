/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.controlingexecution.exercises.ex02;

import java.util.Random;

/**
 *
 * @author isamsonov
 */
public class PrintRandomValues
{
    public static void main(String[] args) {
        Random randomGenerator = new Random(100);

        int[] values = new int[25];

        for (int i = 0; i<25; i++)
        {
            values[i] = randomGenerator.nextInt(100);
        }

        for (int i = 0; i<25; i++)
        {
            if ( values[i] < values[1])
            {
                System.out.println("values " + values[i] + " less than " + values[1]);
            }
            if ( values[i] > values[1])
            {
                System.out.println("values " + values[i] + " more than " + values[1]);
            }
            if ( values[i] == values[1])
            {
                System.out.println("values " + values[i] + " equal " + values[1]);
            }
        }

    }



}
