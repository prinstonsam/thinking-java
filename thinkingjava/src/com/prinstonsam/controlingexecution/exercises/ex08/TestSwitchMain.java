/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.controlingexecution.exercises.ex08;

/**
 *
 * @author isamsonov
 */
public class TestSwitchMain {

    public static void main(String[] args) {
        for (int i = 0; i < 4; i++) {
            switch (i) {
                case 0:
                    System.out.println("is 0");
                    break;
                case 1:
                    System.out.println("is 1");
                    break;
                case 2:
                    System.out.println("is 2");
                    break;
                case 3:
                    System.out.println("is 3");
                    break;
                default:
                    throw new AssertionError();
            }
        }
    }
}
