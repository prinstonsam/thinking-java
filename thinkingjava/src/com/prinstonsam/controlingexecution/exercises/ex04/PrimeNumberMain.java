/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.controlingexecution.exercises.ex04;

/**
 *
 * @author isamsonov
 */
public class PrimeNumberMain {
    public static void main(String[] args) {
        int currentPrimeNumber = 3;

        while( currentPrimeNumber < 3600 )
        {
            currentPrimeNumber = getNextPrimeNumber(currentPrimeNumber);
            System.out.println(currentPrimeNumber);
        }


    }

    private static int getNextPrimeNumber(int currentPrimeNumber)
    {
        for (int j = currentPrimeNumber + 1; ; j++)
        {
            boolean isPrime = true;
            for (int i = 2; i < j; i++)
            {
                if ( j%i == 0)
                {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime)
            {
                return j;
            }
        }
    }

}
