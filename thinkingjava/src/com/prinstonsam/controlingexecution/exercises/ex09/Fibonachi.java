/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.controlingexecution.exercises.ex09;

/**
 *
 * @author isamsonov
 */
public class Fibonachi {
    private int prevValue = 1;
    private int curValue = 1;

    public Fibonachi( int prevValue, int curValue) {
        this.prevValue = prevValue;
        this.curValue = curValue;
    }

    public Fibonachi() {
    }

    public int getPrevValue() {
        return prevValue;
    }

    public void setPrevValue(int prevValue) {
        this.prevValue = prevValue;
    }

    public int getCurValue() {
        return curValue;
    }

    public void setCurValue(int curValue) {
        this.curValue = curValue;
    }

    public int nextValue()
    {
        int tmp = this.curValue;
        this.curValue = this.curValue + this.prevValue;
        this.prevValue = tmp;
        return this.curValue;

    }

}
