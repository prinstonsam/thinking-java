/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.controlingexecution.exercises.ex09;

/**
 *
 * @author isamsonov
 */
public class FibonachiMain {

    public static void main(String[] args)
    {
        Fibonachi fib = new Fibonachi(1,1);

        int value = fib.nextValue();
        while (value < 100){
            value = fib.nextValue();
            System.out.println(value);
        }
    }
}
