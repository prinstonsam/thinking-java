/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.kato.factory;

/**
 *
 * @author isamsonov
 */
interface Service {
    void method1();
}
interface AbstractFactory {
    Service getService();
}
class First implements Service{

    @Override
    public void method1() {
        System.out.println("method1");
    }
}
class FirstImplementation implements AbstractFactory{

    @Override
    public Service getService() {
        return new First();
    }
}
class Second implements Service{
    @Override
    public void method1() {
        System.out.println("second");
    }
}
class SecondImplementation implements AbstractFactory{

    @Override
    public Service getService() {
        return new Second();
    }
}
public class FactoryTest {
    public static void consumerFactories(AbstractFactory factory){
        factory.getService().method1();
    }
    public static void main(String[] args) {
        consumerFactories(new FirstImplementation());
        consumerFactories(new SecondImplementation());
   }
}
