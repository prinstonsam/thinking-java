/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.kato.strategy;

/**
 *
 * @author isamsonov
 */

interface Dog {
    void bark();
}

class BigDog implements Dog{

    @Override
    public void bark() {
        System.out.println("WOW WOW");
    }
}

class SmallDog implements Dog{

    @Override
    public void bark() {
        System.out.println("af af");
    }

}


public class StrategyPrimer {

    static void testDog(Dog dog)
    {
        dog.bark();
    }

    public static void main(String[] args) {
        Dog bigDog = new BigDog();
        Dog smallDog = new SmallDog();

        testDog(bigDog);
        testDog(smallDog);
    }

}
