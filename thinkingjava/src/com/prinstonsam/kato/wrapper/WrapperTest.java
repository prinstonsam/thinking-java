/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.kato.wrapper;

import java.util.Random;

/**
 *
 * @author isamsonov
 */
interface Duck {
    public void quack();
    public void fly();
}
interface Turkey {
    public void gobble();
    public void fly();
}
class DuckAdapter implements Turkey {
    Duck duck;
    Random rand;
    public DuckAdapter(Duck duck) {
        this.duck = duck;
        rand = new Random();
    }
    public void gobble() {
        duck.quack();
    }
    public void fly() {
        if (rand.nextInt(5) == 0) {
            duck.fly();
        }
    }
}
class MallardDuck implements Duck {
    public void quack() {
        System.out.println("Quack");
    }
    public void fly() {
        System.out.println("I'm flying");
    }
}
public class WrapperTest {
    public static void main(String[] args) {
        MallardDuck duck = new MallardDuck();
        Turkey duckAdapter = new DuckAdapter(duck);

        System.out.println("The DuckAdapter says...");
        duckAdapter.gobble();
        duckAdapter.fly();
    }
}
