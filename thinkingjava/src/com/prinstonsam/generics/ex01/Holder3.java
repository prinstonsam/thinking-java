package com.prinstonsam.generics.ex01;

/**
 Exercise 1:   (1) Use Holder3 with the typeinfo.pets library to show that a Holder3
 that is specified to hold a base type can also hold a derived type.
 */
import typeinfo.pets.*;

public class Holder3<T> {
	private T a;

	public Holder3(T a) {
		this.a = a;
	}

	public void set(T a) {
		this.a = a;
	}

	public T get() {
		return a;
	}

	public static void main(String[] args) {
		Holder3<Cat> h3 =
				new Holder3<>(new Cat());
		Cat a = h3.get(); // No cast needed
		// h3.set("Not an Automobile"); // Error
		// h3.set(1); // Error

		System.out.println(a.toString());
	}
}
