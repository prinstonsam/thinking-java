package com.prinstonsam.generics.ex03;

/**
 Exercise 3:  (1) Create and test a SixTuple generic.
 */
import com.prinstonsam.rtti.ex15.*;
import typeinfo.pets.*;
import typeinfo.pets.Cat;
import typeinfo.pets.Cymric;
import typeinfo.pets.Dog;
import typeinfo.pets.EgyptianMau;
import typeinfo.pets.Hamster;
import typeinfo.pets.Manx;
import typeinfo.pets.Mouse;

public class SixTupleTest {

	static SixTuple<Cat,Dog,Cymric,EgyptianMau, Hamster,Manx, Mouse> get(){
		return new SixTuple<>(new Cat(), new Dog(), new Cymric(), new EgyptianMau(), new Hamster(), new Manx(), new Mouse());
	}

	public static void main(String[] args) {
		System.out.println(get());
	}
}
