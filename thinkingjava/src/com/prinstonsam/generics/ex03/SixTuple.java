package com.prinstonsam.generics.ex03;

/**
 * Created by isamsonov on 12/29/15.
 */
public class SixTuple<A,B,C,D,E,F,G> {
	A a;
	B b;
	C c;
	D d;
	E e;
	F f;
	G g;

	public SixTuple(A a, B b, C c, D d, E e, F f, G g){
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
		this.e = e;
		this.f = f;
		this.g = g;
	}
}
