package com.prinstonsam.generics.ex04;

import java.util.ArrayList;

/**

 */
interface Selector
{
	public boolean end();
	public Object current();
	public void next();
}

public class Sequence01<T> {

	private ArrayList<T> items;
	private Integer next = 0;

	public Sequence01(Integer size) {
		items = new ArrayList<>(size);

	}
	public void add(T x) {
		items.add(x);
	}

	private class SequenceSelector implements Selector{
		private int i = 0;
		@Override
		public boolean end() {
			return i == items.size();
		}

		@Override
		public T current() {
			return items.get(i);
		}

		@Override
		public void next() {
			if (i < items.size()){
				i++;
			}
		}
	}

	public Selector selector (){
		return new SequenceSelector();
	}
}
