package com.prinstonsam.generics.ex06;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by isamsonov on 1/12/16.
 */
public class RandomList<T> {
	private ArrayList<T> storage = new ArrayList<T>();

	private Random rand = new Random(47);

	public void add(T item) { storage.add(item); }

	public T select() {
		return storage.get(rand.nextInt(storage.size()));
	}

	public static void main(String[] args) {
		RandomList<String> rs = new RandomList<String>();
		for(String s: ("The quick brown fox jumped over " +
				"the lazy brown dog").split(" "))
			rs.add(s);
		for(int i = 0; i < 11; i++)
			System.out.print(rs.select() + " ");

		RandomList<Integer> rs1 = new RandomList<Integer>();
		for(Integer iVal: new Integer[] {1,2,3,4,5,6,7,8,9,0})
			rs1.add(iVal);
		for(int i = 0; i < 11; i++)
			System.out.print(rs1.select() + " ");

	}
}
