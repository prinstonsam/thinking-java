package com.prinstonsam.generics.ex02;

/**
 * Created by isamsonov on 12/29/15.
 */

import typeinfo.pets.*;

public class MyStorageTest {
	public static void main(String[] args) {
		MyStorage<Cat> cats = new MyStorage<>(new Cat(), new Cat(), new Cat());

		System.out.println(cats.getObj1());
		System.out.println(cats.getObj2());
		System.out.println(cats.getObj3());

	}

}
