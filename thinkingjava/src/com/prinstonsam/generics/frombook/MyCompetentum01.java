package com.prinstonsam.generics.frombook;

import java.util.HashMap;

/**
 * Created by isamsonov on 1/15/16.
 */
public class MyCompetentum01 {

	public static boolean isTheSameCharacters(String a, String b) {

		HashMap<Character, Integer> map = new HashMap<>();
		for (int i = 0; i < a.length(); i++) {
			if (map.containsKey(a.charAt(i))) {
				Integer count = map.get(a.charAt(i));
				count++;
				map.put(a.charAt(i), count);
			}
		}

		HashMap<Character, Integer> map1 = new HashMap<>();
		for (int i = 0; i < b.length(); i++) {
			if (map1.containsKey(b.charAt(i))) {
				Integer count = map1.get(b.charAt(i));
				count++;
				map.put(b.charAt(i), count);
			}
		}

		return map1.equals(map);

	}

	public static void main(String[] args) {
		System.out.println(isTheSameCharacters("aaabbb", "ababa"));
	}


}
