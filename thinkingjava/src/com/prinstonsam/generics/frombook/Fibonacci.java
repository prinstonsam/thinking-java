package com.prinstonsam.generics.frombook;

/**
 * Created by isamsonov on 1/15/16.
 */
public class Fibonacci implements Generator<Integer>{
	private int count = 0;

	@Override
	public Integer next() {
		return fib(count++);
	}

	public Integer fib(int n) {
		if (n<2) return 1;

		return fib(n-2) + fib(n-1);
	}

	public static void main(String[] args) {
		Fibonacci gen = new Fibonacci();
		System.out.println(gen.fib(30) + " ");
	}
}
