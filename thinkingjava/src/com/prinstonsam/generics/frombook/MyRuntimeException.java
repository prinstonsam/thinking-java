package com.prinstonsam.generics.frombook;

import java.io.IOException;
import java.security.AccessControlException;
import java.util.InputMismatchException;

/**
 * Created by isamsonov on 1/15/16.
 */
public class MyRuntimeException {
	public static RuntimeException wrap(Throwable throwable) {
		if (throwable instanceof RuntimeException) {
			return (RuntimeException) throwable;
		}
		return new RuntimeException(throwable.getMessage(), throwable);
	}


	public static void main(String[] args) {

		try{
			throw new RuntimeException();
		} catch (Throwable e) {
			System.out.println("aaa");;
		}

		System.out.println("bbb");
	}

}
