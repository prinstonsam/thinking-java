package com.prinstonsam.generics.frombook;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by isamsonov on 1/11/16.
 */
class Parent{
	protected static Map<Character, String[]> nonValidateProperties;
	static {
		nonValidateProperties = new HashMap<Character, String[]>();

		nonValidateProperties.put('I',
				new String[]{
						"AAA",
						"BBB"
				});
	}
}

public class MyStaticBlockWithInheritance extends Parent{
	static {
		nonValidateProperties = new HashMap<Character, String[]>();

		nonValidateProperties.put('I',
				new String[]{
						"CCC",
						"FFF"
				});
	}

	public static void main(String[] args) {
		MyStaticBlockWithInheritance mmm = new MyStaticBlockWithInheritance();
	}



}



