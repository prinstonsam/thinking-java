package com.prinstonsam.generics.frombook;

import java.util.*;

/**
 * Created by isamsonov on 1/18/16.
 */
class MyClass implements Iterable{
	List<Integer> lst = new ArrayList<>();

	public MyClass(List<Integer> lst) {
		this.lst = lst;
		this.current = lst.size();
	}

	int current;
	@Override
	public Iterator iterator() {

		return new Iterator() {
			@Override
			public boolean hasNext() {
				return current != 0;
			}

			@Override
			public Object next() {
				current--;
				return lst.get(current);
			}
		};

	}

}

public class TestCollections {
	public static void main(String[] args) {
		List<Integer> lst = new ArrayList<>();

		Random rnd = new Random(47);

//		for (int i = 0; i < 10; i++) {
//			lst.add(rnd.nextInt(10));
//		}
//
//		for (Integer item : lst) {
//			System.out.println(item);
//		}
//
//		System.out.println();
//
//		Iterator iter = new MyClass(lst).iterator();
//		while(iter.hasNext())
//		{
//			System.out.println(iter.next());
//		}
//
//		System.out.println(lst.isEmpty());
//
		List<Integer> lnkLst = new LinkedList<>();
		for (int i = 0; i < 10; i++) {
			lnkLst.add(rnd.nextInt(10));
		}

		ListIterator<Integer> lstIter = lnkLst.listIterator();
		while (lstIter.hasPrevious()) {
			System.out.println(lstIter.previous());
		}

		System.out.println();

//		while (lstIter.hasNext()) {
//			System.out.println(lstIter.next());
//		}




	}
}
