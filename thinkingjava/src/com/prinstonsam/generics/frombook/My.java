package com.prinstonsam.generics.frombook;

import java.util.*;

/**
 * Created by isamsonov on 1/11/16.
 */

class ForwarderWithContract {
	Integer forwarderId;
	Integer contractId;

	public Integer getForwarderId() {
		return forwarderId;
	}

	public Integer getContractId() {
		return contractId;
	}

	public void setForwarderId(Integer forwarderId) {
		this.forwarderId = forwarderId;
	}

	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}

	public ForwarderWithContract(Integer forwarderId, Integer contractId) {
		this.forwarderId = forwarderId;
		this.contractId = contractId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		ForwarderWithContract that = (ForwarderWithContract) o;

		if (!forwarderId.equals(that.forwarderId)) return false;
		return contractId.equals(that.contractId);

	}

	@Override
	public int hashCode() {
		int result = forwarderId.hashCode();
		result = 31 * result + contractId.hashCode();
		return result;
	}
}


	public class My {

	public static void main(String[] args) {

		ForwarderWithContract frw = new ForwarderWithContract(1,1);
		HashMap<ForwarderWithContract, List<Integer>> map1 = new HashMap<>();
		List list1 = new ArrayList<>();
		list1.add(100);
		list1.add(200);
		list1.add(300);
		map1.put(frw,list1);


		ForwarderWithContract frw1 = new ForwarderWithContract(1,1);
		HashMap<ForwarderWithContract, List<Integer>> map2 = new HashMap<>();
		List list2 = new ArrayList<>();
		list2.add(1000);
		list2.add(2000);
		list2.add(3000);
		map2.put(frw1,list2);

		TreeMap<ForwarderWithContract, List<Integer>> treeMap =
				new TreeMap<>(new Comparator<ForwarderWithContract>() {
					@Override
					public int compare(ForwarderWithContract o1, ForwarderWithContract o2) {
						return o1.getForwarderId() - o2.getForwarderId();
					}
				});
		treeMap.putAll(map1);

		ForwarderWithContract frw2 = new ForwarderWithContract(1,1);
		if (treeMap.containsKey(frw2))
		{
			System.out.println("haha");
/*
			List<Integer> sourceList = treeMap.get(frw2);

			List<Integer> insertingList = map2.get(frw2);
			sourceList.addAll(insertingList);
*/

			treeMap.get(frw2).addAll(map2.get(frw2));
		}
//		treeMap.putAll(map2);

		for (Map.Entry<ForwarderWithContract, List<Integer>> entry: treeMap.entrySet()) {

			for(Integer detail : entry.getValue()) {
				System.out.println("Key:" + entry.getKey().getForwarderId() + " detail:" + detail);
			}

		}

	}

}
