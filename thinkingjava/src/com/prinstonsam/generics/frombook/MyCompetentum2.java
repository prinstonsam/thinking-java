package com.prinstonsam.generics.frombook;


import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by isamsonov on 1/15/16.
 */
public class MyCompetentum2 {
		public static <T extends Comparable> void sort(T[] a)
		{
			Arrays.sort((T[]) a, 0, a.length, null);
		}


	public static void main(String[] args) {

		Integer[] integers = new Integer[]{2,1,3};
		sort(integers);

		System.out.println(Arrays.toString(integers));


	}

}
