/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.accesscontrol.ex06;

/**
 *
 * @author isamsonov
 */
public class Main {
    public static void main(String[] args) {
        DemoClass01 demo01 = new DemoClass01();

        demo01.print();


        DemoClass03.printStaticS();

        DemoClass03 demo03 = new DemoClass03("constructor initialization");

        demo03.printNonStaticS();
    }

}
