/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.accesscontrol.ex06;

/**
 *
 * @author isamsonov
 */
public class DemoClass03 {

    private static String s = "default place";

    {
        s = "initialization block";
    }

    public DemoClass03(String value) {

        System.out.println(s);

        s = value;

        System.out.println(s);

    }

    public static void printStaticS(){
        System.out.println(s);
    }

    public void printNonStaticS(){
        System.out.println(s);
    }


}
