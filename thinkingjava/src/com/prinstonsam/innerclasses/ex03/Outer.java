/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.innerclasses.ex03;

/**
 *
 * @author isamsonov
 */
public class Outer {

    private String myString;

    class Inner{

        public Inner() {
            System.out.println("inner");
        }

        @Override
        public String toString() {
            return myString;
        }
    }

    public Outer(String myString) {
        this.myString = myString;
    }

    public Inner inner(){
        return new Inner();
    }

    public static void main(String[] args) {
        Outer o = new Outer("MyString");

        System.out.println(o.inner().toString());
    }

}
