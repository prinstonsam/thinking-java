/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.innerclasses.ex12;

/**
 *
 * @author isamsonov
 */
interface InnerInterface{
    public void innerMethod(String value);
}

class Outer{
    private String outField;
    private void outMethod1(){
        System.out.println(outField);
    }

    InnerInterface inner(){

        return new InnerInterface(){

            @Override
            public void innerMethod(String value) {
                Outer.this.outField=value;
                Outer.this.outMethod1();
            }

        };
    }
}

public class Main {
    public static void main(String[] args) {
        Outer o = new Outer();
        o.inner().innerMethod("Haha");
    }

}
