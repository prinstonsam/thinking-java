/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.innerclasses.ex18;

import com.prinstonsam.innerclasses.ex18.Outer.Nested;

/**
 *
 * @author isamsonov
 */

class Outer{
    public static class Nested{
        int i =0;

        public int getI() {
            return i;
        }


    }
}

public class Main {

    public static void main(String[] args) {
        Nested n = new Nested();
        System.out.println(n.getI());
    }


}
