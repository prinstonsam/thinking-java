/*
 * A class defined within a method
 */
package com.prinstonsam.innerclasses.ex11;

/**
 *
 * @author isamsonov
 */
interface MyInterface {
    public String methodFromInterface();
}

public class Outer {

    private class Inner implements MyInterface {

        @Override
        public String methodFromInterface() {
            return "Haha";
        }

        public String tryGetIt(){
            return "Yeee tgi ???";
        }
    }

    public MyInterface getInner(){
        return new Outer.Inner();
    }

    public static void main(String[] args) {
        Outer o = new Outer();

        System.out.println(o.getInner().methodFromInterface());
//        System.out.println(o.getInner().tryGetIt); // Don't get private member
    }
}
