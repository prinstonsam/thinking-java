/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.innerclasses.ex08;

/**
 *
 * @author isamsonov
 */
class Outer{
    private String outField;
    class Inner{
        private void privateInnerMetod(){
            System.out.println("private method");
        }
    }
    Inner inner(){
        return new Inner();
    }
    public void outMethod2(){
        Outer.Inner inner = inner();
        inner.privateInnerMetod(); //RIGHT
    }
}
public class Main {
    public static void main(String[] args) {
        Outer o = new Outer();
//        o.inner().privateInnerMethod(); //WRONG
    }
}
