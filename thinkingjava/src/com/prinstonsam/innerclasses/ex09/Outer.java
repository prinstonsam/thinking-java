/*
* A class defined within a method
 */
package com.prinstonsam.innerclasses.ex09;

/**
 *
 * @author isamsonov
 */
interface MyInterface{
    public String methodFromInterface();
}
public class Outer {
    public MyInterface outerMethod(){
        class InnerWithinMethod implements MyInterface {
            @Override
            public String methodFromInterface() {
                return "Haha";
            }
        }
        return new InnerWithinMethod();
    }
    public static void main(String[] args) {
        Outer o = new Outer();

        System.out.println(o.outerMethod().methodFromInterface());
    }
}
