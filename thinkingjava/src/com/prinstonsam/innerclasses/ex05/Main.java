/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.innerclasses.ex05;

/**
 *
 * @author isamsonov
 */
class Base{
    class Inner{
        @Override
        public String toString() {
            return "Inner";
        }
    }
}
public class Main {
    public static void main(String[] args) {
        Base base = new Base();
        Base.Inner inner = base.new Inner();
        System.out.println(inner.toString());
    }
}
