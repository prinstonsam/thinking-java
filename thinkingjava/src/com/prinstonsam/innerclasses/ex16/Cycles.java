/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.innerclasses.ex16;

/**
 *
 * @author isamsonov
 */
interface Cycle{
    void move();
}
interface CycleFactory{
    Cycle getCycle();
}


class Unicycle implements Cycle{
    private Unicycle(){}
    @Override
    public void move() {
        System.out.println("by uni");
    }
    public static CycleFactory uniCycleFactory = new CycleFactory() {

        @Override
        public Cycle getCycle() {
            return new Unicycle();
        }
    };
}

class Bicycle implements Cycle{
    private Bicycle(){}
    @Override
    public void move() {
        System.out.println("by bi");
    }
    public static CycleFactory biCycleFactory = new CycleFactory() {
        @Override
        public Cycle getCycle() {
            return new Bicycle();
        }
    };
}

class Tricycle implements Cycle{
    private Tricycle(){}
    @Override
    public void move() {
        System.out.println("by tri");
    }

    public static CycleFactory triCycleFactory = new CycleFactory() {

        @Override
        public Cycle getCycle() {
            return new Tricycle();
        }
    };
}

public class Cycles {
    public static void moveCycle(CycleFactory cf){
        Cycle c = cf.getCycle();
        c.move();
    }


    public static void main(String[] args) {
        moveCycle(Bicycle.biCycleFactory);
        moveCycle(Unicycle.uniCycleFactory);
        moveCycle(Tricycle.triCycleFactory);

    }

}
