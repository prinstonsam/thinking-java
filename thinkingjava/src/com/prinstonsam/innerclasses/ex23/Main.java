/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.innerclasses.ex23;

import java.util.ArrayList;

/**
 *
 * @author isamsonov
 */

//(4) Create an interface U with three methods. Create a class
//A with a method that produces a reference to a U by building an anonymous
//inner class. Create a second class B that contains an array of U. B should
//have one method that accepts and stores a reference to a U in the array, a
//second method that sets a reference in the array (specified by the method
//argument) to null, and a third method that moves through the array and
//calls the methods in U. In main( ), create a group of A objects and a single
//B. Fill the B with U references produced by the A objects. Use the B to call
//back into all the A objects. Remove some of the U references from the B.

interface U {
    void method1();
    void method2();
    void method3();
}

class A {
    String name;

    U implem1(){
        return new U() {

            @Override
            public void method1() {
                System.out.println("implem1.method1");
            }

            @Override
            public void method2() {
                System.out.println("implem1.method2");
            }

            @Override
            public void method3() {
                System.out.println("implem1.method3");
            }
        };
    }

    public A(String name) {
        this.name = name;
    }
}

class B{
    ArrayList<U> items = new ArrayList<>();

    void add(U u){
        items.add(u);
    }

    U get(int i){
        return items.get(i);
    }
}



public class Main {

    public static void main(String[] args) {
        B b = new B();
        for (int i = 0; i < 10; i++) {
            A a = new A("A num " +i);
            b.add(a.implem1());
        }


        for (U item: b.items) {

            item.method1();
            item.method2();
            item.method3();

        }
    }




}
