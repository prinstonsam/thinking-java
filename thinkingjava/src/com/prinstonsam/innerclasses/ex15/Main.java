/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.innerclasses.ex15;

/**
 *
 * @author isamsonov
 */

abstract class Outer1{
    public Outer1(int x){
    }

    abstract public void method1();
}

class Outer2{
    public Outer1 outer1(int x){
        return new Outer1(x) {
            public void method1(){
                System.out.println(x);
            }
        };
    }
}

public class Main {
    public static void main(String[] args) {
        Outer2 o = new Outer2();

        o.outer1(100).method1();

    }
}


