    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.innerclasses.anonymous.ex02;

/**
 *
 * @author isamsonov
 */
abstract class Base{
    private int x;
    public Base(int x){
        this.x = x;
    }
    abstract void baseMethod();

    public int getX() {
        return x;
    }

}
public class AnonymousConstructor {
    public static Base getBase(int x){
        return new Base(x) {
            {
                System.out.println("Initialization");
            }
            @Override
            void baseMethod() {
                System.out.println("Overriding basemethod");
            }
        };
    }
    public static void main(String[] args) {
        AnonymousConstructor ac = new AnonymousConstructor();

        ac.getBase(100).getX();
    }
}
