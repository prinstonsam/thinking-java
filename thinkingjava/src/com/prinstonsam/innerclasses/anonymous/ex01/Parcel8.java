/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.innerclasses.anonymous.ex01;

/**
 *
 * @author isamsonov
 */
interface Contents {
    public int value();
}
public class Parcel8 {
    public Contents contents() {
        return new Contents() { // Insert a class definition
            private int i = 11;

            public int value() {
                return i;
            }
        }; // Semicolon required in this case
    }
    public static void main(String[] args) {
        Parcel8 p = new Parcel8();
        Contents c = p.contents();
    }
} //

