/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.innerclasses.ex14;
//: interfaces/HorrorShow.java
// Extending an interface with inheritance.

interface Monster {
    void menace();
}

interface DangerousMonster extends Monster {
    void destroy();
}

interface Lethal {
    void kill();
}

class DragonZilla {
    public DangerousMonster getDangerousMonster() {
        return new DangerousMonster() {

            @Override
            public void destroy() {
                System.out.println("destroy.DangerousMonster");
            }

            @Override
            public void menace() {
                System.out.println("menace.DangerousMonster");
            }
        };
    }
}

interface Vampire extends DangerousMonster, Lethal {

    void drinkBlood();
}

class VeryBadVampire {
    Vampire getVampire = new Vampire() {

        @Override
        public void drinkBlood() {
            System.out.println("drinkBlood.Vampire");
        }

        @Override
        public void destroy() {
            System.out.println("destroy.Vampire");
        }

        @Override
        public void menace() {
            System.out.println("menace.Vampire");
        }

        @Override
        public void kill() {
            System.out.println("menace.Vampire");
        }
    };

}

public class HorrorShow {

    static void u(Monster b) {
        b.menace();
    }

    static void v(DangerousMonster d) {
        d.menace();
        d.destroy();
    }

    static void w(Lethal l) {
        l.kill();
    }

    public static void main(String[] args) {
        DangerousMonster barney = new DragonZilla().getDangerousMonster();
        barney.destroy();
        barney.menace();
        Vampire vlad = new VeryBadVampire().getVampire;
        vlad.destroy();
        vlad.drinkBlood();
        vlad.menace();
        vlad.kill();
    }
} ///:~

