/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.innerclasses.ex25;

/**
 *
 * @author isamsonov
 */
public class NewGreenhouseControls extends GreenhouseControls{
    private boolean waterMist = false;

    public class WaterMistGeneratorOn extends Event {

        public WaterMistGeneratorOn(long delayTime) {
            super(delayTime);
        }

        public void action() {
// Put hardware control code here to
// physically turn on the fan.
            waterMist = true;
        }

        public String toString() {
            return "WaterMistGeneratorOn is on";
        }
    }

    public class WaterMistGeneratorOff extends Event {

        public WaterMistGeneratorOff(long delayTime) {
            super(delayTime);
        }

        public void action() {
// Put hardware control code here to
// physically turn on the fan.
            waterMist = false;
        }

        public String toString() {
            return "WaterMistGeneratorOn is off";
        }
    }
}
