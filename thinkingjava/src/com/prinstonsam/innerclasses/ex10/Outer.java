/*
 * A class defined within a method
 */
package com.prinstonsam.innerclasses.ex10;

/**
 *
 * @author isamsonov
 */
interface MyInterface {

    public String methodFromInterface();
}

public class Outer {

    public MyInterface outerMethod(boolean flag) {
        if (flag) {
            class InnerWithinMethod implements MyInterface {

                @Override
                public String methodFromInterface() {
                    return "Haha";
                }
            }
            return new InnerWithinMethod();
        }
        return null;
    }

    public static void main(String[] args) {
        Outer o = new Outer();

        System.out.println(o.outerMethod(true).methodFromInterface());
    }
}
