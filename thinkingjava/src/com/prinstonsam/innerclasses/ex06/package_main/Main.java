/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.innerclasses.ex06.package_main;

import com.prinstonsam.innerclasses.ex06.package_impl_interface.ImplemInner;
import com.prinstonsam.innerclasses.ex06.package_interface.MyInterfaceInner;


/**
 *
 * @author isamsonov
 */

class Inherited extends ImplemInner{
    public void invokeMethod1(MyInterfaceInner value){
        value.method1();
    }
}
public class Main{
    public static void main(String[] args) {
        Inherited impl = new Inherited();
        impl.invokeMethod1(impl.getInner());
    }
}
