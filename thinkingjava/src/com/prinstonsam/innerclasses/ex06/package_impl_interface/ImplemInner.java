/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.innerclasses.ex06.package_impl_interface;

import com.prinstonsam.innerclasses.ex06.package_interface.MyInterfaceInner;

/**
 *
 * @author isamsonov
 */
public class ImplemInner {
    protected class Inner implements MyInterfaceInner{
        @Override
        public void method1() {
            System.out.println("method1");
        }
    }
    public Inner getInner(){
        return new Inner();
    }
}
