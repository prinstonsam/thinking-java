/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.innerclasses.ex21;

/**
 *
 * @author isamsonov
 */
class ImplOuter implements Outer{

    @Override
    public void print() {
        System.out.println("Ehhho");
    }
}
public interface Outer {
    void print();

    public static class InnerClass{
        public static void impl(Outer outer){
            outer.print();
        }

        public static void main(String[] args) {
            impl(new ImplOuter());
        }
    }
}


