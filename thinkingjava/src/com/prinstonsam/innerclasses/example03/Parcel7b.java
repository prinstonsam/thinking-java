/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.innerclasses.example03;

/**
 *
 * @author isamsonov
 */

interface Contents {
    public int value();
}
public class Parcel7b {

//    class MyContents implements Contents {
//
//        private int i = 11;
//
//        @Override
//        public int value() {
//            return i;
//        }
//    }

    public Contents contents() {
        return new Contents() {
            private int i = 11;

            @Override
            public int value() {
                return i;
            }
        };
    }

    public static void main(String[] args) {
        Parcel7b p = new Parcel7b();
        Contents c = p.contents();
        System.out.println(c.value());
    }
} ///:~

