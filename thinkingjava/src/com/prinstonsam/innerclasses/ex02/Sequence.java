/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.innerclasses.ex02;

/**
 *
 * @author isamsonov
 */
interface Selector
{
    public boolean end();
    public Object current();
    public void next();
}

public class Sequence {

    private Object [] items;
    private Integer next = 0;

    public Sequence(Integer size) {
        items = new Object[size];

    }
    public void add(Object x) {
        if(next < items.length){
            items[next++] = x;
        }
    }

    private class SequenceSelector implements Selector{
        private int i = 0;
        @Override
        public boolean end() {
            return i == items.length;
        }

        @Override
        public Object current() {
            return items[i];
        }

        @Override
        public void next() {
            if (i < items.length){
                i++;
            }
        }
    }

    public Selector selector (){
        return new SequenceSelector();
    }
}
