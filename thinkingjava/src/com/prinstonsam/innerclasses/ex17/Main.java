/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.innerclasses.ex17;

import java.util.Random;

/**
 *
 * @author isamsonov
 */
interface Game{
    int tossing();
}

interface GameFactory{
    Game getGame();
}

class Coins implements Game{

    @Override
    public int tossing() {
        Random rnd = new Random(100);
        int r;
        do
            r = rnd.nextInt(11);
        while( r == 0 );

        rnd.nextInt(11);
        return r;
    }

    public static GameFactory factory = new GameFactory() {
        @Override
        public Game getGame() {
            return new Coins();
        }
    };
}

class Dice implements Game{
    @Override
    public int tossing() {
        Random rnd = new Random(100);
        int r;
        do
            r = rnd.nextInt(13);
        while( r == 0 );

        rnd.nextInt(11);
        return r;
    }
    public static GameFactory factory = new GameFactory() {
        @Override
        public Game getGame() {
            return new Dice();
        }
    };
}


public class Main {

    public static void playGame(GameFactory gameFactory){
        Game s = gameFactory.getGame();
        System.out.println(s.tossing());
    }
    public static void main(String[] args) {
        playGame(Dice.factory);
        playGame(Coins.factory);
    }


}
