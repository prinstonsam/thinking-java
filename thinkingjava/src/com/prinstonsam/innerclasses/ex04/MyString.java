/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.innerclasses.ex04;

/**
 *
 * @author isamsonov
 */
public class MyString {
    private String item;

    public MyString(String item) {
        this.item = item;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    @Override
    public String toString(){
        return item;
    }

}
