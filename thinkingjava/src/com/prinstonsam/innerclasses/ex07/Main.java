/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.innerclasses.ex07;

/**
 *
 * @author isamsonov
 */
class Outer{
    private String outField;
    private void outMethod1(){
        System.out.println(outField);
    }

    class Inner{
        private String innerField;
        private void privateInnerMetod(){
            System.out.println("private method");
        }
        public void innerMethod(String value){
            Outer.this.outField=value;
            Outer.this.outMethod1();
        }
    }

    Inner inner(){
        return new Inner();
    }

    public void outMethod2(){
        Outer.Inner inner = new Outer.Inner();
        inner.innerMethod("Haha");
        inner.privateInnerMetod();
    }
}

public class Main {
    public static void main(String[] args) {
        Outer o = new Outer();
        o.outMethod2();
    }

}
