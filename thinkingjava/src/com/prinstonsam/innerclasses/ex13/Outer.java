/*
* A class defined within a method
 */
package com.prinstonsam.innerclasses.ex13;

/**
 *
 * @author isamsonov
 */
interface MyInterface{
    public String methodFromInterface();
}
public class Outer {
    public MyInterface outerMethod(){
        return new MyInterface() {

            @Override
            public String methodFromInterface() {
                return "Haha";
            }
        };
    }
    public static void main(String[] args) {
        Outer o = new Outer();

        System.out.println(o.outerMethod().methodFromInterface());
    }
}
