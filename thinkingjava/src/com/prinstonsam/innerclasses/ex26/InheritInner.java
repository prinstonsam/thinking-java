/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.innerclasses.ex26;

/**
 *
 * @author isamsonov
 */
class WithInner {
    class Inner {
        public Inner(String str){
        }
    }
}
public class InheritInner extends WithInner.Inner {
//! InheritInner() {} // Won't compile
    InheritInner(WithInner wi) {
        wi.super("Haha");
    }
    public static void main(String[] args) {
        WithInner wi = new WithInner();
        InheritInner ii = new InheritInner(wi);
    }
} ///:~

