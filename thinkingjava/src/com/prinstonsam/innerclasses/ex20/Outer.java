/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.innerclasses.ex20;

/**
 *
 * @author isamsonov
 */
public interface Outer {
    void print();

    public static class Inner implements Outer{

        @Override
        public void print() {
            System.out.println("Innener");
        }

        public static void main(String[] args) {
            Inner inner = new Inner();

            inner.print();
        }


    }

}
