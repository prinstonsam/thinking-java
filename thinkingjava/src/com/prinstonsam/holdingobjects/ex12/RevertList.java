package com.prinstonsam.holdingobjects.ex12;

/*
Exercise 12: (3) Create and populate a List<Integer>. Create a second
        List<Integer> of the same size as the first, and use ListIterators to read
        elements from the first List and insert them into the second in reverse order.
        (You may want to explore a number of different ways to solve this problem.)
*/

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

public class RevertList {
    public static void main(String[] args) {
        List<Integer> sourceList = new LinkedList<>();
        Random rnd = new Random();
        for (int i=0; i<10; i++) {
            sourceList.add(rnd.nextInt(10));
        }

        List<Integer> destList = new LinkedList<>();

        ListIterator<Integer> sourceIterator = sourceList.listIterator();

        while (sourceIterator.hasNext()) {
            System.out.println(("Sourcelist: " + sourceIterator.next()));
        }

        while (sourceIterator.hasPrevious()) {
            destList.add(sourceIterator.previous());
        }

        for (Integer value : destList) {
            System.out.println(("Destinationlist: " + value));
        }

    }
}
