package com.prinstonsam.holdingobjects.ex18;

import com.sun.corba.se.impl.encoding.OSFCodeSetRegistry;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by isamsonov on 10/21/15.
 * Exercise 18: (3) Fill a HashMap with key-value pairs. Print the results
 * to show ordering by hash code. Extract the pairs, sort by key, and place the
 * result into a LinkedHashMap. Show that the insertion order is maintained.
 */

public class Main {
    public static void main(String[] args) {
        Random rand  = new Random(90);

        HashMap<Integer, Float> hashMap = new HashMap<>();

        for (int i = 0; i < 30; i++) {
            hashMap.put(rand.nextInt(10), rand.nextFloat());
        }

        System.out.println(hashMap);

        LinkedHashMap<Integer, Float> linkedHashMap = new LinkedHashMap<>();

        for (Map.Entry e : hashMap.entrySet()){
            linkedHashMap.put((Integer) e.getKey(), (float) e.getValue());
        }

        System.out.println(linkedHashMap);
    }
}
