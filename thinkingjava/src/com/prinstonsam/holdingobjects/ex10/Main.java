/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.holdingobjects.ex10;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author isamsonov
 */
public class Main {

    public static void printAll(ArrayList<Rodent> rodents){

        Iterator iterator = rodents.iterator();

        while(iterator.hasNext()) {
            ;
            System.out.println(((Rodent)iterator.next()).getSize());

        }

    }

    public static void main(String[] args) {
        ArrayList<Rodent> rodents = new ArrayList(10);
        RandomRodentGenerator fabric = new RandomRodentGenerator();

        for (int i = 0; i < 10; i++) {
            rodents.add(fabric.next());
        }

        printAll(rodents);
    }

}
