package com.prinstonsam.holdingobjects.ex15;

/**
 * Created by isamsonov on 10/12/15.
 */
public class Main {
    private static final String VALUE= "+U+n+c---+e+r+t---+a-+i-+n+t+y---+ -+r+u--+l+e+s---";

    public static void main(String[] args) {
        if (VALUE.length() == 0) {
            return;
        }

        Stack<Character> stack = new Stack<>();

        for (int i = 0; i < VALUE.length() - 1; i++) {
            if (VALUE.charAt(i) == '-') {
                System.out.println(stack.pop());
            }
            if (VALUE.charAt(i) == '+') {
                stack.push(VALUE.charAt(i + 1));
            }
        }
    }
}
