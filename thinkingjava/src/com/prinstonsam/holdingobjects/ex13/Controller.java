/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.holdingobjects.ex13;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


/**
 *
 * @author isamsonov
 */
public class Controller {
// A class from java.util to hold Event objects:
    private List<Event> eventList = new LinkedList<Event>();

    public void addEvent(Event c) {
        eventList.add(c);
    }

    public void run() {
        while (eventList.size() > 0) // Make a copy so you're not modifying the list
        // while you're selecting the elements in it:
        {
            for (Event e : new LinkedList<Event>(eventList)) {
                if (e.ready()) {
                    System.out.println(e);
                    e.action();
                    eventList.remove(e);
                }
            }
        }
    }
} ///:~
