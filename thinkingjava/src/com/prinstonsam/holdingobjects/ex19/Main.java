package com.prinstonsam.holdingobjects.ex19;

import java.util.*;

/**
 * Created by isamsonov on 10/21/15.
 * Exercise 18: (3) Fill a HashMap with key-value pairs. Print the results
 * to show ordering by hash code. Extract the pairs, sort by key, and place the
 * result into a LinkedHashMap. Show that the insertion order is maintained.
 */

public class Main {
    public static void main(String[] args) {
        Random rand  = new Random(90);

        HashSet<Integer> hashSet = new HashSet<>();

        for (int i = 0; i < 30; i++) {
            hashSet.add(rand.nextInt(10));
        }

        System.out.println(hashSet);

        LinkedHashSet<Integer> linkedHashSet = new LinkedHashSet<>();

        for (Integer e : hashSet){
            linkedHashSet.add(e);
        }

        System.out.println(linkedHashSet);
    }
}
