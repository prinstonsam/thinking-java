package com.prinstonsam.holdingobjects.ex29;

/*
Exercise 29: (2) Create a simple class that inherits from Object and
        contains no members, and show that you cannot successfully add multiple
        elements of that class to a PriorityQueue. This issue will be fully explained
        in the Containers in Depth chapter.
*/

import java.util.PriorityQueue;

class First extends Object{
}

public class Main {
    public static void main(String[] args) {

        PriorityQueue<Object> pq = new PriorityQueue<>();

        pq.add(new First());
        pq.add(new First());

/*        Exception in thread "main" java.lang.ClassCastException:
                com.prinstonsam.holdingobjects.ex29.First cannot be cast to java.lang.Comparable
        at java.util.PriorityQueue.siftUpComparable(PriorityQueue.java:652)
        at java.util.PriorityQueue.siftUp(PriorityQueue.java:647)
        at java.util.PriorityQueue.offer(PriorityQueue.java:344)
        at java.util.PriorityQueue.add(PriorityQueue.java:321)
        at com.prinstonsam.holdingobjects.ex29.Main.main(Main.java:21)
        at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
        at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
        at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
        at java.lang.reflect.Method.invoke(Method.java:497)
        at com.intellij.rt.execution.application.AppMain.main(AppMain.java:140)*/
    }
}
