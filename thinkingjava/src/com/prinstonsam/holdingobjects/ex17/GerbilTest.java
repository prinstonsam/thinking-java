/*
Exercise 1: (2) Create a new class called Gerbil with an int
gerbilNumber that’s initialized in the constructor. Give it a method called
hop( ) that displays which gerbil number this is, and that it’s hopping. Create
an ArrayList and add Gerbil objects to the List. Now use the get( )
method to move through the List and call hop( ) for each Gerbil.

 */
package com.prinstonsam.holdingobjects.ex17;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author isamsonov
 */

class Gerbil {
    private int gerbilNumber = 0;

    public int getGerbilNumber() {
        return this.gerbilNumber;
    }

    public void setGerbilNumber(int gerbilNumber) {
        this.gerbilNumber = gerbilNumber;
    }

    public Gerbil(int gerbilNumber) {
        this.gerbilNumber = gerbilNumber;
    }

    public int hop() {
        return this.gerbilNumber;
    }
}

public class GerbilTest {

    public static void main(String[] args) {

        Map<String, Gerbil> mapGerbils = new HashMap<>();

        String [] nameGerbils = {"ha", "hu", "buska", "garry", "soso"};

        for (int i = 0; i < 5; i++) {
            Gerbil gerbil = new Gerbil(i);
            mapGerbils.put(nameGerbils[i], gerbil);
        }

        for (String key : mapGerbils.keySet()) {
            System.out.println(key + " " + mapGerbils.get(key).hop());
        }
    }

}
