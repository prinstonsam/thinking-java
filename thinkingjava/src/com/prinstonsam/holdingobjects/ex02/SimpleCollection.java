/*
Exercise 2: (1) Modify SimpleCollection.java to use a Set for c.

 */
package com.prinstonsam.holdingobjects.ex02;

import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author isamsonov
 */
public class SimpleCollection {

    public static void main(String[] args) {
        Collection<Integer> c = new HashSet<>();
        for (int i = 0; i < 10; i++) {
            c.add(i); // Autoboxing
        }
        for (Integer i : c) {
            System.out.print(i + ", ");
        }
    }
}
