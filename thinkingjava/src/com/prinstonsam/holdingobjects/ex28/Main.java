package com.prinstonsam.holdingobjects.ex28;

/*
Exercise 28: (2) Fill a PriorityQueue (using offer( )) with Double
        values created using java.util.Random, then remove the elements using
        poll( ) and display them.
*/

import java.util.PriorityQueue;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        PriorityQueue<Double> pq = new PriorityQueue<>();

        Random rnd = new Random(100);
        for (int i = 0; i < 5; i++) {
            pq.offer(rnd.nextDouble());
        }


        while (pq.peek() != null) {
            System.out.println(pq.poll());
        }
    }
}
