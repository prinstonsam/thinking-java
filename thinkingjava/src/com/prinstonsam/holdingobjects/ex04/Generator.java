package com.prinstonsam.holdingobjects.ex04;

import java.util.*;

/**
 * Created by isamsonov on 10/7/15.
 */
interface GenerateStrategy {
    void generate(Phrases phrases);
    void show();
}

class Phrases{
    public final String [] items = {"Никогда не говори никогда",
            "Как жить в стране без цветных штанов",
            "Скрипач не нужен",
            "Пока космические корабли бороздят просторы",
            "Ицых с гвоздями"
    };
}

class ListImpl {
    Collection<String> array;

    public ListImpl(Collection array) {
        this.array = array;
    }

    GenerateStrategy strategy = new GenerateStrategy() {
        @Override
        public void generate(Phrases phrases) {
            for (String item : phrases.items) {
                array.add(item);
            }
        }

        @Override
        public void show() {
            for (String item : array) {
                System.out.println(item);
            }
        }
    };
}

public class Generator {

    public static void main(String[] args) {

        ListImpl al = new ListImpl(new ArrayList());
        ListImpl ll = new ListImpl(new LinkedList());
        ListImpl hs = new ListImpl(new HashSet());
        ListImpl ts = new ListImpl(new TreeSet());
        ListImpl lhs = new ListImpl(new LinkedHashSet());


        Phrases phrases = new Phrases();

        al.strategy.generate(phrases);
        ll.strategy.generate(phrases);
        hs.strategy.generate(phrases);
        ts.strategy.generate(phrases);
        lhs.strategy.generate(phrases);

        al.strategy.show();
        ll.strategy.show();
        hs.strategy.show();
        ts.strategy.show();
        lhs.strategy.show();
    }


}
