package com.prinstonsam.holdingobjects.ex14;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * Created by isamsonov on 10/12/15.
 */
public class MiddleAdd {
    LinkedList<String> ll = new LinkedList<>();

    public void addItem(String item) {
        int middle = ll.size()/2;

        ListIterator<String> it = ll.listIterator();
        for (int i = 0; i < middle; i++) {
            if (it.hasNext()) {
                it.next();
            }
        }
        it.add(item);
    }
}
