package com.prinstonsam.holdingobjects.ex21;

/*
Exercise 21: (3) Using a Map<String,Integer>, follow the form of
        UniqueWords.java to create a program that counts the occurrence of
        words in a file. Sort the results using Collections.sort( ) with a second
        argument of String.CASE_INSENSITIVE_ORDER (to produce an
        alphabetic sort), and display the result.
*/

import java.util.*;

public class UniqueWords {
    public static void main(String[] args) {
        LinkedList<String> allWords = new LinkedList<>(new TextFile("SetOperations.java", "\\W+"));

        HashMap<String, Integer> mapCountEachWord = new HashMap<>();

        int countInFile = 0;
        for(String word : allWords){
            Integer countOfWord = mapCountEachWord.get(word);
            mapCountEachWord.put(word, countOfWord == null ? 1 : countOfWord + 1);
        }

        LinkedList ll = new LinkedList();

        ll.addAll(mapCountEachWord.keySet());

        Collections.sort(ll, String.CASE_INSENSITIVE_ORDER);

        System.out.println("Sorted list: " + ll);
    }
}
