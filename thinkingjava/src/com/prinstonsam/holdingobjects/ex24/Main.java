package com.prinstonsam.holdingobjects.ex24;

/*
Exercise 24: (2) Fill a LinkedHashMap with String keys and objects
        of your choice. Now extract the pairs, sort them based on the keys, and
        reinsert them into the Map.
*/

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;

class Car{
    private String model;
    private String type;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Car(String model, String type) {
        this.model = model;
        this.type = type;
    }
}

public class Main {

    public static void main(String[] args) {
        LinkedHashMap<String, Car> listCars = new LinkedHashMap<>();

        Car first = new Car("Vaz2101", "passenger car");
        Car second = new Car("Kamaz", "truk");
        Car third = new Car("Volvo", "bus");

        listCars.put("555", first);
        listCars.put("222", second);
        listCars.put("111", third);

        LinkedList<String> ll = new LinkedList<>(listCars.keySet());

        ll.sort((o1, o2) -> o1.compareTo(o2));

        LinkedHashMap<String, Car> newListCars = new LinkedHashMap<>();
        for(String key : ll){
            newListCars.put(key, listCars.get(key));
        }

        System.out.println(listCars);
        System.out.println(newListCars);

    }
}
