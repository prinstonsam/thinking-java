package com.prinstonsam.holdingobjects.ex27;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

/*
Exercise 27: (2) Write a class called Command that contains a String
        and has a method operation( ) that displays the String. Write a second
class with a method that fills a Queue with Command objects and returns
        it. Pass the filled Queue to a method in a third class that consumes the
        objects in the Queue and calls their operation( ) methods.
*/
class Command{
    private String field;

    public Command(String field) {
        this.field = field;
    }

    public void operation(){
        System.out.println(field);
    }
}

class Second{
    private String [] valueField = {"first", "second", "third", "fourth", "fifth"};

    private Queue<Command> queue;

    public Queue<Command> getQueue(){
        return queue;
    }

    public void fill(int capacity){
        queue = new LinkedList<Command>();

        Random rnd = new Random(100);
        for (int i = 0; i < capacity; i++) {
            queue.add(new Command(valueField[rnd.nextInt(5)]));
        }
    }
}

class Consumer{
    public static void consume(Queue<Command> queue){
        while ( queue.peek() != null ){
            queue.poll().operation();
        }
    }
}


public class Main {
    public static void main(String[] args) {
        Second second = new Second();

        second.fill(100);

        Consumer.consume(second.getQueue());
    }
}
