/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.holdingobjects.ex09;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author isamsonov
 */
interface Selector
{
    public boolean end();
    public Object current();
    public void next();
}

public class Sequence {

    private ArrayList items;
    private Integer next = 0;


    public void add(Object obj){
        items.add(obj);
    }

    public Sequence(Integer size) {
        items = new ArrayList(size);
    }


    private class SequenceSelector implements Selector{
        private int i = 0;
        private Object current;
        Iterator iterator;

        public SequenceSelector(){
            iterator = items.iterator();
        }
        public boolean end() {
            return !iterator.hasNext();
        }

        public Object current() {
            return current;
        }

        public void next() {
            current = iterator.next();
        }

    }

    public Selector selector (){
        return new SequenceSelector();
    }
}
