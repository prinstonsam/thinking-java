/*
Exercise 3: (2) Modify innerclasses/Sequence.java so that you can
add any number of elements to it.

 */
package com.prinstonsam.holdingobjects.ex03;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author isamsonov
 */
interface Selector
{
    public boolean end();
    public Object current();
    public void next();
}

public class Sequence {

    private List<Object> items;
    private Integer next = 0;

    public Sequence(Integer size) {
        items = new ArrayList(size);

    }
    public void add(Object x) {
        items.add(x);
    }

    private class SequenceSelector implements Selector{
        private int i = 0;
        @Override
        public boolean end() {
            return i == items.size()-1;
        }

        @Override
        public Object current() {
            return items.get(i);
        }

        @Override
        public void next() {
            if (i < items.size()){
                i++;
            }
        }
    }

    public Selector selector (){
        return new SequenceSelector();
    }
}
