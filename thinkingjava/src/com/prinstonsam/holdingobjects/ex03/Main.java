/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.holdingobjects.ex03;

/**
 *
 * @author isamsonov
 */
public class Main {
    public static void main(String[] args) {
        Sequence sequence = new Sequence(10);

        for (int i = 0; i < 10; i++) {
            sequence.add(new MyString("String number:" +i));
        }

        Selector selector = sequence.selector();

        while( !selector.end()){
            System.out.println(selector.current().toString());
            selector.next();
        }
    }

}
