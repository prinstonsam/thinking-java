/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.holdingobjects.ex31;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

/**
 *
 * @author isamsonov
 */
public class Shapes {

    private static RandomShapeGenerator gen
            = new RandomShapeGenerator();

    public static void main(String[] args) {
        Collection<Shape> s= new LinkedList<>();

        //Shape[] s = new Shape[9];
// Fill up the array with shapes:
        for (int i = 0; i < 10; i++) {
            s.add(gen.next());
        }
// Make polymorphic method calls:
        for (Shape shp : s) {
            shp.draw();
            shp.rotate();
        }
    }
}

