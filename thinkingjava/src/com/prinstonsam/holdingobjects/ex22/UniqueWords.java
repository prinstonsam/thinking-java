package com.prinstonsam.holdingobjects.ex22;

/*
Exercise 22: (5) Modify the previous exercise so that it uses a class
        containing a String and a count field to store each different word, and a Set
                of these objects to maintain the list of words.
*/

import com.prinstonsam.holdingobjects.ex21.TextFile;

import java.util.*;

public class UniqueWords {
    public static void main(String[] args) {
//        LinkedList<String> allWords = new LinkedList<>(new TextFile("SetOperations.java", "\\W+"));

        String [] arrString = {"aa", "aa", "aa", "bb"};
        List<String> allWords = Arrays.asList(arrString);


        HashMap<String, Integer> mapCountEachWord = new HashMap<>();

        for(String word : allWords){
            Integer countOfWord = mapCountEachWord.get(word);
            mapCountEachWord.put(word, countOfWord == null ? 1 : countOfWord + 1);
        }

        LinkedList ll = new LinkedList();

        ll.addAll(mapCountEachWord.keySet());

        Collections.sort(ll, String.CASE_INSENSITIVE_ORDER);

        System.out.println("Sorted list: " + ll);

        Set<FrequencyWord> setFrequencyWord = new HashSet<>();

        FrequencyWord fw = new FrequencyWord();
        if (allWords.size() > 0){
            fw.setWork((String)allWords.get(0));
            fw.setCount(1);
        }

        for (int i = 1; i < allWords.size(); i++) {
            if (!((String)allWords.get(i)).equals(fw.getWork())){
                setFrequencyWord.add(fw);
                fw = new FrequencyWord();
                fw.setWork((String)allWords.get(i));
                fw.setCount(fw.getCount() == null ? 1 : fw.getCount() + 1);
            }
            else{
                fw.setCount(fw.getCount()+1);
            }
        }

        for(FrequencyWord item : setFrequencyWord )
        {
            if ( item.getCount()>1) {
                System.out.println(item.getWork() + " " + item.getCount());
            }
        }
    }
}
