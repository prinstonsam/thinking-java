package com.prinstonsam.holdingobjects.ex22;

/**
 * Created by isamsonov on 10/23/15.
 */
public class FrequencyWord {
    private String work;
    private Integer count;

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
