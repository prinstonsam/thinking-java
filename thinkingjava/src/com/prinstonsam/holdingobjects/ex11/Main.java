package com.prinstonsam.holdingobjects.ex11;

import java.util.*;

/**
 * Created by isamsonov on 10/8/15.
 */
public class Main {

    public static void method01(Collection list) {
        Iterator iterator = list.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next().toString());
        }

    }

    public static void method02(Collection collection) {
        for (int i = 0; i < 10; i++) {
            collection.add((new StringBuilder()).append("string01").append(" ").append(i).toString());
        }

        Iterator iterator = collection.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next().toString());
        }
    }

    public static void main(String[] args) {
        method02(new ArrayList<String>());
        method02(new LinkedList<String>());
        method02(new TreeSet<String>());


    }

}
