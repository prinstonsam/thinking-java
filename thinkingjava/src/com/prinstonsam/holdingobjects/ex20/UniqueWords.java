package com.prinstonsam.holdingobjects.ex20;

/**
 * Created by isamsonov on 10/12/15.
 */

import java.util.*;

public class UniqueWords {
    public static void main(String[] args) {
        Set<String> words = new TreeSet<String>(
                new TextFile("SetOperations.java", "\\W+"));

        LinkedList<String> allWords = new LinkedList<>(new TextFile("SetOperations.java", "\\W+"));


        Character [] arrayVowels = {'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'};

        Set<Character> vowels = new TreeSet(Arrays.asList(arrayVowels));

        HashMap<String, Integer> mapCountEachWord = new HashMap<>();

        int countInFile = 0;
        for (String word : words) {


            int countInWord = 0;
            for (int i = 0; i < word.length(); i++) {
                if(vowels.contains(word.charAt(i))){
                    countInWord++;
                }
            }
            System.out.println("Word "+word+" contain " + countInWord + " vowels");
            countInFile+=countInWord;
        }

        for(String word : allWords){
            Integer countOfWord = mapCountEachWord.get(word);
            mapCountEachWord.put(word, countOfWord == null ? 1 : countOfWord + 1);
        }

        System.out.println("Summary words: " + countInFile);

        System.out.println("Count each word: " + mapCountEachWord);
    }
}
