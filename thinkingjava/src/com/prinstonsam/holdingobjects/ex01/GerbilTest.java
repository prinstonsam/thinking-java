/*
Exercise 1: (2) Create a new class called Gerbil with an int
gerbilNumber that’s initialized in the constructor. Give it a method called
hop( ) that displays which gerbil number this is, and that it’s hopping. Create
an ArrayList and add Gerbil objects to the List. Now use the get( )
method to move through the List and call hop( ) for each Gerbil.

 */
package com.prinstonsam.holdingobjects.ex01;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author isamsonov
 */

class Gerbil{
    private int gerbilNumber = 0;

    public int getGerbilNumber() {
        return this.gerbilNumber;
    }

    public void setGerbilNumber(int gerbilNumber) {
        this.gerbilNumber = gerbilNumber;
    }

    public Gerbil(int gerbilNumber) {
        this.gerbilNumber = gerbilNumber;
    }

    public int hop(){
        return this.gerbilNumber;
    }
}


public class GerbilTest {

    public static void main(String[] args) {
        List<Gerbil> gerbilList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            Gerbil gerbil = new Gerbil(i);
            gerbilList.add(gerbil);
        }

        for (Gerbil gerbil : gerbilList) {
            System.out.println(gerbil.hop());
        }
    }

}
