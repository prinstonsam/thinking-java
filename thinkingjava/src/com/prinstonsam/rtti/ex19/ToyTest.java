package com.prinstonsam.rtti.ex19;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;

/**
* Exercise 19:    (4) In ToyTest.java, use reflection to create a Toy object using
 * the non-default constructor.
 */



public class ToyTest {

	public static void main(String[] args) {
		Class c = null;
		try {
			c = Class.forName("com.prinstonsam.rtti.ex19.MyToy");

			c = MyToy.class;
			Constructor[] constructor = c.getConstructors();

			Arrays.toString(constructor);

			//MyToy myToy = constructor.newInstance("mytoy");

		} catch (ClassNotFoundException e) {
			System.out.println("Can't find MyFancyToy");
			System.exit(1);

/*
		} catch (NoSuchMethodException e) {
			System.out.println("Can't create MyFancyToy");
			System.exit(1);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
*/
			System.out.println(c.toString());
		}
	}
}
