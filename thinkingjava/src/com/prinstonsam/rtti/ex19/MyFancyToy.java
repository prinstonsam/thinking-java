package com.prinstonsam.rtti.ex19;

/**
 * Created by isamsonov on 12/28/15.
 */
public class MyFancyToy extends MyToy implements MyHasBatteries, MyWaterproof, MyShoots, MyAdded {
	public MyFancyToy() {
		super(1);
	}
}
