package com.prinstonsam.rtti.ex02;

/**
 Exercise 2:  (2) Incorporate a new kind of interface into ToyTest.java and verify that it is detected and displayed properly.
 */


interface MyHasBatteries {}

interface MyWaterproof {}

interface MyShoots {}

interface MyAdded {}

class MyToy {
    // Comment out the following default constructor
    // to see NoSuchMethodError from (*1*)
//    MyToy() {}
    MyToy(int i) {}
}

class MyFancyToy extends MyToy implements MyHasBatteries, MyWaterproof, MyShoots, MyAdded {
    MyFancyToy() { super(1); }
}

public class ToyTest {
    static void printInfo(Class cc) {
        System.out.println("Class name: " + cc.getName() +
                " is interface? [" + cc.isInterface() + "]");
        System.out.println("Simple name: " + cc.getSimpleName());
        System.out.println("Canonical name : " + cc.getCanonicalName());
    }

    public static void main(String[] args) {
        Class c = null;
        try {
            c = Class.forName("com.prinstonsam.rtti.ex02.MyFancyToy");
        } catch(ClassNotFoundException e) {
            System.out.println("Can't find FancyToy");
            System.exit(1);
        }
        printInfo(c);
        for(Class face : c.getInterfaces())
            printInfo(face);
        Class up = c.getSuperclass();
        Object obj = null;
        try {
            // Requires default constructor:
            obj = up.newInstance();
        } catch(InstantiationException e) {
            System.out.println("Cannot instantiate");
            System.exit(1);
        } catch(IllegalAccessException e) {
            System.out.println("Cannot access");
            System.exit(1);
        }
        printInfo(obj.getClass());
    }
}