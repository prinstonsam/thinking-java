package com.prinstonsam.rtti.frombook;

import java.util.Arrays;
import java.util.List;

/**
 * Created by isamsonov on 12/9/15.
 */

abstract class MyShape {
    public void draw()
    {
        System.out.println(toString() + ".toString");
    }
    @Override
    public abstract String toString();
}

class Circle extends MyShape {

    @Override
    public String toString() {
        return "Circle";
    }
}

class Square extends MyShape {

    @Override
    public String toString() {
        return "Square";
    }
}

class Triangle extends MyShape {

    @Override
    public String toString() {
        return "Triangle";
    }
}

public class Shapes {
    public static void main(String[] args) {
        List<MyShape> shapes = Arrays.asList(new Circle(), new Square(), new Triangle());

        for(MyShape shape : shapes){
            shape.draw();
        }
    }
}
