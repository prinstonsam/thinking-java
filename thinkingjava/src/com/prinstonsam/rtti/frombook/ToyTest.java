package com.prinstonsam.rtti.frombook;

/**
 * Created by isamsonov on 12/9/15.
 */
//: typeinfo/toys/ToyTest.java

// Testing class Class.

interface MyHasBatteries {}
interface MyWaterproof {}
interface MyShoots {}

class MyToy {
    // Comment out the following default constructor
    // to see NoSuchMethodError from (*1*)
//    MyToy() {}
    MyToy(int i) {}
}

class MyFancyToy extends MyToy implements MyHasBatteries, MyWaterproof, MyShoots {
    MyFancyToy() { super(1); }
}

public class ToyTest {
    static void printInfo(Class cc) {
        System.out.println("Class name: " + cc.getName() +
                " is interface? [" + cc.isInterface() + "]");
        System.out.println("Simple name: " + cc.getSimpleName());
        System.out.println("Canonical name : " + cc.getCanonicalName());
    }

    public static void main(String[] args) {
        Class c = null;
        try {
            c = Class.forName("com.prinstonsam.rtti.frombook.MyFancyToy");
        } catch(ClassNotFoundException e) {
            System.out.println("Can't find FancyToy");
            System.exit(1);
        }
        printInfo(c);
        for(Class face : c.getInterfaces())
            printInfo(face);
        Class up = c.getSuperclass();
        Object obj = null;
        try {
            // Requires default constructor:
            obj = up.newInstance();
        } catch(InstantiationException e) {
            System.out.println("Cannot instantiate");
            System.exit(1);
        } catch(IllegalAccessException e) {
            System.out.println("Cannot access");
            System.exit(1);
        }
        printInfo(obj.getClass());
    }
}