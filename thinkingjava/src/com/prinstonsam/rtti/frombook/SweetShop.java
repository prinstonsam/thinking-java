package com.prinstonsam.rtti.frombook;

/**
 * Created by isamsonov on 12/9/15.
 */
import static net.mindview.util.Print.*;


class MyCandy{
    static {
        print("Loaded Candy");
    }
}

class MyGum{
    static {
        print("Loaded Gunasfddsafsafd");
    }
}

class MyCookie{
    static {
        print("Loaded Cookie");
    }
}
public class SweetShop {
    public static void main(String[] args) {
        print("Inside main");
        new MyCandy();
        print("After candy");
        try {
            Class.forName("MyGum");
        } catch (ClassNotFoundException ex) {
            print("Can't load GUM");
        }
        print("After gum");
        new MyCookie();
        print("After cookie");


    }

}
