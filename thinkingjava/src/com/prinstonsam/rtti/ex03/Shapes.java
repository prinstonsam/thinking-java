package com.prinstonsam.rtti.ex03;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 Exercise 3:  (2) Add Rhomboid to Shapes.java. Create a Rhomboid, upcast it to a Shape, then downcast
 it back to a  Rhomboid. Try downcasting to a Circle and see what happens.
 Exercise 4:  (2) Modify the previous exercise so that it uses instanceof to check the type before
 performing  the downcast.
 Exercise 5:  (3) Implement a rotate(Shape) method in Shapes.java, such that it checks to see
 if it is rotating a  Circle (and, if so, doesn’t perform the operation).
 Exercise 4:  (2) Modify the previous exercise so that it uses instanceof to check the type
 before performing the downcast.
 Exercise 5:  (3) Implement a rotate(Shape) method in Shapes.java, such that it checks
 to see if it is rotating a Circle (and, if so, doesn’t perform the operation).
 */

abstract class MyShape {
    public void draw()
    {
        System.out.println(toString() + ".toString");
    }
    //ex05
    public void rotate() {}
    @Override
    public abstract String toString();
}

class Circle extends MyShape {

    @Override
    public String toString() {
        return "Circle";
    }
    //ex05
    public void rotate() {
        System.out.println("Circle is rotating");}
}

class Square extends MyShape {

    @Override
    public String toString() {
        return "Square";
    }
}

class Triangle extends MyShape {
    @Override
    public String toString() { return "Triangle";}
}

class Rhomboid extends MyShape {
    @Override
    public String toString() { return "Rhomboid";}
}



public class Shapes {
    //ex05
    public static void rotate(MyShape shape){
        if (shape instanceof Circle)
            ((Circle) shape).rotate();
        }


    public static void main(String[] args) {
        List<MyShape> shapes = Arrays.asList(new Circle(), new Square(), new Triangle());

        for(MyShape shape : shapes){
            shape.draw();
            //ex05
            rotate(shape);
        }

        List<MyShape> shapes1 = new ArrayList<>();

        shapes1.add(new Rhomboid());

        for(MyShape shape : shapes1){
            if (shape instanceof Rhomboid) {
                ((Rhomboid) shape).draw();
            }
        }

//        for(MyShape shape : shapes1){
//            ((Circle)shape).draw(); //java.lang.ClassCastException
//        }

        //ex04
        for(MyShape shape : shapes1){
            if (shape instanceof Circle) {
                ((Circle) shape).draw();
            }
       }

    }
}
