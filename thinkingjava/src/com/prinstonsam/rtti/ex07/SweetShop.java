package com.prinstonsam.rtti.ex07;

/**
 Exercise 7:  (3) Modify SweetShop.java so that each type of object creation is controlled by
 a command-line argument. That is, if your command line is “java SweetShop Candy,”
 then only the Candy object is created. Notice how you can control which Class objects
 are loaded via the command-line argument.
 */

import static net.mindview.util.Print.print;


class MyCandy{
    static {
        print("Loaded Candy");
    }
}

class MyGum{
    static {
        print("Loaded Gunasfddsafsafd");
    }
}

class MyCookie{
    static {
        print("Loaded Cookie");
    }
}

public class SweetShop {
    public static void main(String[] args) {
        if (args.length == 0){
            System.out.println("You didn't have args");
            System.exit(1);
        }
        try {
            Class.forName("com.prinstonsam.rtti.ex07."+args[0]);
        } catch (ClassNotFoundException ex) {
            print("Can't load " + args[0]);
        }
        print("After " + args[0]);
    }

}
