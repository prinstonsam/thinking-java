package com.prinstonsam.rtti.ex26;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by isamsonov on 12/28/15.
 */

interface MyOperationForInstrument{
	public void clearSpitValue();
}

class Wind extends MyInstrument{
	public void prepareInstrument() {
		System.out.println("Wind prepare");
	}
}

class MyElectronic extends MyInstrument{
	public void prepareInstrument() {
		System.out.println("MyElectronic prepare");
	}
}

public class MyInstrument{
	public void prepareInstrument(){}

}

class Test {
	static List<MyInstrument>  instruments = new ArrayList<>();
	static{
				Collections.addAll(instruments,
						new MyElectronic(),
						new Wind());
	}
	public static void main(String[] args) {

		for(MyInstrument inst : instruments)
			inst.prepareInstrument();
	}
}




