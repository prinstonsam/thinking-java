package com.prinstonsam.rtti.ex09;

import java.lang.reflect.Field;

/**
 Exercise 9:  (5) Modify the previous exercise so that it uses Class.getDeclaredFields( )
 to also display information about the fields in a class.
 */
class My1{
	protected int field01;

}

class My2 extends My1 {
	protected int field02;

}

class My3 extends My2 {
	private int field03;

}

public class ShowAllClassInHierarchy {
	public static void main(String[] args) {
		Class myClassIter = My3.class;

		while( myClassIter != null ) {
			for (Field deField : myClassIter.getDeclaredFields()) {
				System.out.println(deField);
			}
			myClassIter = myClassIter.getSuperclass();
		}
	}
}
