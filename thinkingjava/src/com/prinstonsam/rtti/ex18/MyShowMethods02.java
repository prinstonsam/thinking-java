package com.prinstonsam.rtti.ex18;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.regex.Pattern;

/**
 * Exercise 18:    (1) Make ShowMethods a non-public class and verify that the synthesized
 * default constructor no longer shows up in the output.
 * It possible if we will use getDeclaredConstuctors
 */
class MyNonPublic {
/*	public MyNonPublic() {
	}*/
}

public class MyShowMethods02 {

	public static void main(String[] args) {
		Class<?> c = MyNonPublic.class;
		Method[] methods = c.getMethods();
		Constructor[] ctors = c.getDeclaredConstructors();
/*
		for (Method method : methods) {
			System.out.println(p.matcher(method.toString()).replaceAll(""));
		}
*/
		for (Constructor ctor : ctors) {
			System.out.println(ctor.toString());
		}
	}
}
