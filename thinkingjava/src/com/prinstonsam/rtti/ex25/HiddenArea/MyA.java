package com.prinstonsam.rtti.ex25.HiddenArea;

/**
 * Created by isamsonov on 12/28/15.
 */
public class MyA{
	public void publicMethod(){
		System.out.println("public method");
	}
	protected void protectedMethod(){
		System.out.println("protected method");
	}
	private void privateMethod(){
		System.out.println("private method");
	}

}
