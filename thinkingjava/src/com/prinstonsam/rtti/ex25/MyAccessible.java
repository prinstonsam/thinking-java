package com.prinstonsam.rtti.ex25;

import com.prinstonsam.rtti.ex25.HiddenArea.MyA;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MyAccessible {
	public static void main(String[] args) {
		Class clazz = MyA.class;

		Method[] methods = clazz.getDeclaredMethods();
		MyA myA = null;
		try {
			myA= (MyA)clazz.getDeclaredConstructor().newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}

		for( Method method : methods ) {
			method.setAccessible(true);
			try {
				method.invoke(myA);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
	}

}
