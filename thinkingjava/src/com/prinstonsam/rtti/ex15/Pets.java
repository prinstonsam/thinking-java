//: typeinfo/pets/Pets.java
// Facade to produce a default PetCreator.
package com.prinstonsam.rtti.ex15;

import java.util.ArrayList;
import java.util.List;

public class Pets {
	public static final PetCreator creator =
			new LiteralPetCreator();

	public static Pet randomPet() {
		return creator.randomPet();
	}

	public static List<Pet> createArray(int size) {
		return creator.createArray(size);
	}

/*	public static ArrayList<Pet> arrayList(int size) {
		return creator.arrayList(size);
	}*/
} ///:~
