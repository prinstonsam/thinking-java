//: typeinfo/pets/PetCreator.java
// Creates random sequences of Pets.
package com.prinstonsam.rtti.ex15;

import typeinfo.factory.Factory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public abstract class PetCreator {
	// The List of the different types of Pet to create:
	public abstract List<Class<? extends Pet>> types();


	static List<Factory<? extends Pet>> partFactories =
			new ArrayList<>();

	static
	{ Collections.addAll(
					partFactories,
					new Cat.Factory(),
					new Cymric.Factory());
	}

	public Pet randomPet() { // Create one random Pet
		int n = rand.nextInt(types().size());
		try {
			return types().get(n).newInstance();
		} catch (InstantiationException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	private static Random rand = new Random(47);
	public static Pet createRandom(){
		int n = rand.nextInt(partFactories.size());
		return partFactories.get(n).create();
	}

	public  static List<Pet> createArray(int size){
		List<Pet> pets = new ArrayList<>(size);
		for (int i = 0; i < size; i++) {
			pets.add(createRandom());
		}

		return pets;
	}
}
