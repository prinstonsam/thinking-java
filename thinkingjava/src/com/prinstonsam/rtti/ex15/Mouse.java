//: typeinfo/pets/Mouse.java
package com.prinstonsam.rtti.ex15;

public class Mouse extends Rodent {
	public Mouse(String name) {
		super(name);
	}

	public Mouse() {
		super();
	}

	public static class Factory implements typeinfo.factory.Factory{

		@Override
		public Mouse create() {
			return new Mouse();
		}
	}

} ///:~
