package com.prinstonsam.rtti.ex14;

import typeinfo.factory.Factory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 Exercise 14:    (4) A constructor is a kind of factory method. Modify RegisteredFactories.java
 so that instead of using an explicit factory, the class object is stored in the List,
 and newInstance( ) is used to create each object.
 */


class Part {
	public String toString(){
		return getClass().getSimpleName();
	}

	static List<Factory<? extends Part>> partFactories =
			new ArrayList<>();

	static {
		Collections.addAll(partFactories,
				new FuelFilter.Factory(),
				new AirFilter.Factory(),
				new OilFilter.Factory(),
				new CabinAirFilter.Factory(),
				new FanBelt.Factory(),
				new PowerSteeringBelt.Factory(),
				new GeneratorBelt.Factory());
/*
		partFactories.add(new FuelFilter.Factory());
		partFactories.add(new AirFilter.Factory());
		partFactories.add(new OilFilter.Factory());
		partFactories.add(new CabinAirFilter.Factory());
		partFactories.add(new FanBelt.Factory());
		partFactories.add(new PowerSteeringBelt.Factory());
		partFactories.add(new GeneratorBelt.Factory());
*/
	}

	private static Random rand = new Random(47);
	public static Part createRandom(){
		int n = rand.nextInt(partFactories.size());
		try {
			return (Part)partFactories.get(n).getClass().newInstance().create();  //for ex14
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}
}

class Filter extends Part {}

class FuelFilter extends Filter {
	public static class Factory
			implements typeinfo.factory.Factory<FuelFilter> {

		@Override
		public FuelFilter create() {
			return new FuelFilter();
		}
	}
}

class AirFilter extends Filter {
	public static class Factory
			implements typeinfo.factory.Factory<AirFilter> {
		@Override
		public AirFilter create() {
			return new AirFilter();
		}
	}
}

class CabinAirFilter extends Filter {
	public static class Factory
			implements typeinfo.factory.Factory<CabinAirFilter> {
		@Override
		public CabinAirFilter create() {
			return new CabinAirFilter();
		}
	}
}

class OilFilter extends Filter {
	public static class Factory
			implements typeinfo.factory.Factory<OilFilter> {

		@Override
		public OilFilter create() {
			return new OilFilter();
		}
	}
}

class Belt extends Part {}

class FanBelt extends Belt {
	public static class Factory implements typeinfo.factory.Factory<FanBelt> {

		@Override
		public FanBelt create() {
			return new FanBelt();
		}
	}
}

class GeneratorBelt extends Belt {
	public static class Factory implements typeinfo.factory.Factory<GeneratorBelt> {

		@Override
		public GeneratorBelt create() {
			return new GeneratorBelt();
		}
	}
}

class PowerSteeringBelt extends Belt {
	public static class Factory implements typeinfo.factory.Factory<PowerSteeringBelt> {

		@Override
		public PowerSteeringBelt create() {
			return new PowerSteeringBelt();
		}
	}
}

public class RegisteredFactories {
	public static void main(String[] args) {
		for (int i = 0; i < 10; i++) {
			System.out.println(Part.createRandom());
		}
	}
}
