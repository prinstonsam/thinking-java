package com.prinstonsam.rtti.ex08;

/**
 Exercise 8:  (5) Write a method that takes an object and recursively prints all the classes
 in that object’s hierarchy.
 */
class My1{

}

class My2 extends My1{

}

class My3 extends My2{

}

public class ShowAllClassInHierarchy {
	public static void main(String[] args) {
		Class myClassIter = My3.class;

		while( myClassIter != null ){
			System.out.println( myClassIter );
			myClassIter = myClassIter.getSuperclass();
		}
	}


}
