/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.everythingisanobject.exercises.ex09;

/**
 *
 * @author isamsonov
 */
public class Main {
    public static void main(String[] args) {

        boolean boolPrim = true;
        Boolean boolWrap = boolPrim;
        System.out.println(boolWrap.getClass());

        byte bytePrim = 100;
        Byte byteWrap = bytePrim ;
        System.out.println(byteWrap.getClass());

        short shortPrim = 1000;
        Short shortWrap = shortPrim;
        System.out.println(shortWrap.getClass());

        int intPrim = 1000;
        Integer intWrap = intPrim;
        System.out.println(intWrap.getClass());

        long longPrim = 999999;
        Long longWrap = longPrim;
        System.out.println(longWrap.getClass());

        float floatPrim = 9999;
        Float floatWrap = floatPrim;
        System.out.println(floatWrap);
        System.out.println(floatWrap.getClass());

        double doublePrim = 999999.6666;
        Double doubleWrap = doublePrim;
        System.out.println(doubleWrap.getClass());

    }
}
