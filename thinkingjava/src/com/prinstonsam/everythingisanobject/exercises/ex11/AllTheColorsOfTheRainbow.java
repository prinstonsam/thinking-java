/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.everythingisanobject.exercises.ex11;

/**
 *
 * @author isamsonov
 */


class AllTheColorsOfTheRainbow {

  int anIntegerRepresentingColors;
  public void changeTheHueOfTheColor(int newHue) {
      anIntegerRepresentingColors = newHue;
  }

  void printTheHueOfTheColor()
  {
      System.out.println("the hue of the color: " + anIntegerRepresentingColors );
  }

    public int getAnIntegerRepresentingColors() {
        return anIntegerRepresentingColors;
    }

    public void setAnIntegerRepresentingColors(int anIntegerRepresentingColors) {
        this.anIntegerRepresentingColors = anIntegerRepresentingColors;
    }

}

