/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.everythingisanobject.exercises.ex11;

/**
 *
 * @author isamsonov
 */
public class Main {
    public static void main(String[] args) {

        AllTheColorsOfTheRainbow atcotr = new AllTheColorsOfTheRainbow();

        System.out.println(atcotr.getAnIntegerRepresentingColors());

        atcotr.changeTheHueOfTheColor(100);

        System.out.println(atcotr.getAnIntegerRepresentingColors());

    }


}
