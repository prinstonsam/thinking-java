/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.everythingisanobject.exercises.ex08;

/**
 *
 * @author isamsonov
 */
public class SourceClass {

    public static int value;

    public static int getValue() {
        return value;
    }

    public static void setValue(int value) {
        SourceClass.value = value;
    }

}
