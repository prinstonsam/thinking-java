/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.everythingisanobject.exercises.ex08;

import java.util.ArrayList;

/**
 *
 * @author isamsonov
 */
public class Main {
    public static void main(String[] args) {
        ArrayList<SourceClass> sourceClasses =  new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            SourceClass sourceClass = new SourceClass();

            sourceClasses.add(sourceClass);
        }

        SourceClass.setValue(100);

        sourceClasses.stream().forEach((sourceClass) -> {
            System.out.println(sourceClass.getValue());
        });
        System.out.println("SourceClass.value:" + SourceClass.getValue());

        SourceClass.setValue(200);
        sourceClasses.stream().forEach((sourceClass) -> {
            System.out.println(sourceClass.getValue());
        });
    }

}
