package com.prinstonsam.everythingisanobject.exercises.ex02;

/**
 * My first program
 */
import java.util.*;

public class HelloDate {
   /** Entry point to class & application.
   * @param args array of string arguments
   * @throws exceptions No exceptions thrown
  */
  public static void main(String[] args) {
    System.out.println("Hello, it's: ");
    System.out.println(new Date());
  }

}
