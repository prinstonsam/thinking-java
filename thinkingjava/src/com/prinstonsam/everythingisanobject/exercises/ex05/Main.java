/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.everythingisanobject.exercises.ex05;

/**
 *
 * @author isamsonov
 */
public class Main {
    public static void main(String[] args) {
        DataOnly dataOnly = new DataOnly();

        dataOnly.i = 100;
        dataOnly.d = 100.678;
        dataOnly.b = false;

        System.out.println(dataOnly.i);
        System.out.println(dataOnly.d);
        System.out.println(dataOnly.b);
    }
}
