/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.everythingisanobject.exercises.ex01;

/**
 *
 * @author isamsonov
 */
public class Main {
    public static void main(String[] args) {

        Class01 c = new Class01();

        System.out.println(c.intValue);
        System.out.println(c.charValue);

        c.staticIntValue = 100;

        Class01 b = new Class01();

        b.staticIntValue = 200;

        System.out.println(Class01.staticCharValue);
        System.out.println(c.staticIntValue);

    }




}
