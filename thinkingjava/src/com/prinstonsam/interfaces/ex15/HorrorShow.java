/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.interfaces.ex15;

/**
 *
 * @author isamsonov
 */
abstract class Monster {

    abstract void menace();
}

abstract class DangerousMonster extends Monster {

    abstract void destroy();
}

interface Lethal {

    void kill();
}

class DragonZilla extends DangerousMonster {

    @Override
    public void menace() {
        System.out.println("DangerousMonster.menace");
    }

    @Override
    public void destroy() {
        System.out.println("DangerousMonster.destroy");
    }
}

interface Vampire extends Lethal {

    void drinkBlood();
}

class VeryBadVampire extends DangerousMonster implements Vampire {

    @Override
    public void menace() {
        System.out.println("VaryBadVampire.menace");
    }

    @Override
    public void destroy() {
        System.out.println("VaryBadVampire.destroy");
    }

    @Override
    public void kill() {
        System.out.println("VaryBadVampire.kill");
    }

    @Override
    public void drinkBlood() {
        System.out.println("VaryBadVampire.drinkBlood");
    }
}

public class HorrorShow {

    static void u(Monster b) {
        b.menace();
    }

    static void v(DangerousMonster d) {
        d.menace();
        d.destroy();
    }

    static void w(Lethal l) {
        l.kill();
    }

    public static void main(String[] args) {
        DangerousMonster barney = new DragonZilla();
        u(barney);
        v(barney);
        Vampire vlad = new VeryBadVampire();
        u((Monster)vlad);
        v((DangerousMonster)vlad);
        w(vlad);
    }
} ///:~
