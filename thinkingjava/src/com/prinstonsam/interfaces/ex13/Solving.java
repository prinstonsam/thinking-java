/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.interfaces.ex13;

/**
 *
 * @author isamsonov
 */
interface First{


}
interface Second extends First{


    void method2();

}
interface Third extends First, Second{
    void method3();
}

class A implements First{


    public void method1() {
        System.out.println("Firstl.method1");
    }

}

class B implements Second{


    public void method1() {
        System.out.println("Second.method1");
    }

    @Override
    public void method2() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}


public class Solving implements Third{

    @Override
    public void method3() {
    }

//    @Override
//    public void method1() {
//    }

    @Override
    public void method2() {
    }

}
