/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.interfaces.ex10;

import com.prinstonsam.polymorphism.Note;

/**
 *
 * @author isamsonov
 */
interface Playable
{
    void play(Note n); // Automatically public
}

 abstract class Instrument {
// Compile-time constant:

    int VALUE = 5; // static & final
// Cannot have method definitions:


    abstract void adjust();
}

class Wind extends Instrument implements Playable{

    public void play(Note n) {
        System.out.println(this + ".play() " + n);
    }

    public String toString() {
        return "Wind";
    }

    public void adjust() {
        System.out.println(this + ".adjust()");
    }
}

class Percussion extends Instrument implements Playable{

    public void play(Note n) {
        System.out.println(this + ".play() " + n);
    }

    public String toString() {
        return "Percussion";
    }

    public void adjust() {
        System.out.println(this + ".adjust()");
    }
}

class Stringed extends Instrument implements Playable{

    public void play(Note n) {
        System.out.println(this + ".play() " + n);
    }

    public String toString() {
        return "Stringed";
    }

    public void adjust() {
        System.out.println(this + ".adjust()");
    }
}

class Brass extends Wind {

    public String toString() {
        return "Brass";
    }
}

class Woodwind extends Wind {

    public String toString() {
        return "Woodwind";
    }
}

public class Music5 {
// Doesn't care about type, so new types
// added to the system still work right:

    static void tune(Playable p) {
// ...
        p.play(Note.MIDDLE_C);
    }

    static void tuneAll(Playable[] e) {
        for (Playable p : e) {
            tune(p);
        }
    }

    public static void main(String[] args) {
// Upcasting during addition to the array:
        Playable[] orchestra = {
            new Wind(),
            new Percussion(),
            new Stringed(),
            new Brass(),
            new Woodwind()
        };
        tuneAll(orchestra);
    }
}
