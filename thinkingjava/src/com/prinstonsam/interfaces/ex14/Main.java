/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.interfaces.ex14;

import java.util.Scanner;

/**
 *
 * @author isamsonov
 */
interface First{
    void method1();

}
interface Second{
    void method2();

}
interface Third{
    void method3();

}

interface Fourth extends First, Second, Third{
    void method4();

}

class Derived implements Fourth {

    @Override
    public void method4() {
        System.out.println("method4");
    }

    @Override
    public void method1() {
        System.out.println("method1");
    }

    @Override
    public void method2() {
        System.out.println("method2");
    }

    @Override
    public void method3() {
        System.out.println("method3");
    }

}


public class Main {
    public static void main(String[] args) {
        First f = new Derived();
        f.method1();
        Second s = new Derived();
        s.method2();
        Third t = new Derived();
        t.method3();
        Fourth four = new Derived();
        four.method1();
        four.method2();
        four.method3();
        four.method4();
    }

}
