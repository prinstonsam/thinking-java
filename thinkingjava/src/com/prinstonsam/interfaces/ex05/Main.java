/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.interfaces.ex05;

import com.prinstonsam.interfaces.ex05.implpackage.Implem;

/**
 *
 * @author isamsonov
 */
public class Main {
    public static void main(String[] args) {
        Implem implem = new Implem();

        implem.method01();
        implem.method02();
        implem.method03();
    }

}
