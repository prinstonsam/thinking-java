/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.interfaces.ex05.implpackage;

import com.prinstonsam.interfaces.ex05.interfacepackage.MyInterface;

/**
 *
 * @author isamsonov
 */
public class Implem implements MyInterface{

    @Override
    public void method01() {
        System.out.println("method1");
    }

    @Override
    public void method02() {
        System.out.println("method2");
    }

    @Override
    public void method03() {
        System.out.println("method3");
    }

}
