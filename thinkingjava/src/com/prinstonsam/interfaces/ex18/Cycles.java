/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.interfaces.ex18;

/**
 *
 * @author isamsonov
 */
interface Cycle{
    void move();
}
interface CycleFactory{
    Cycle getCycle();
}


class Unicycle implements Cycle{

    @Override
    public void move() {
        System.out.println("by uni");
    }
}

class UnicycleFactory implements CycleFactory{
    @Override
    public Cycle getCycle() {
        return new Unicycle();
    }
}

class Bicycle implements Cycle{

    @Override
    public void move() {
        System.out.println("by bi");
    }
}
class BicycleFactory implements CycleFactory{
    @Override
    public Cycle getCycle() {
        return new Bicycle();
    }
}

class Tricycle implements Cycle{

    @Override
    public void move() {
        System.out.println("by tri");
    }
}

class TricycleFactory implements CycleFactory{
    @Override
    public Cycle getCycle() {
        return new Tricycle();
    }
}


public class Cycles {
    public static void moveCycle(CycleFactory cf){
        Cycle c = cf.getCycle();
        c.move();
    }


    public static void main(String[] args) {
        moveCycle(new BicycleFactory());
        moveCycle(new UnicycleFactory());
        moveCycle(new TricycleFactory());

    }

}
