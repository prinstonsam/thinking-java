/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.initializationandcleanup.ex14;

/**
 *
 * @author isamsonov
 */
public class ClassTest {

    static int staticField = 101;

    int nonStaticField = 100;

    static void showStatic(){
        System.out.println(staticField);
    }

    void showNonStatic(){
        System.out.println(nonStaticField);
    }

}

class Main {
    public static void main(String[] args) {

        ClassTest.showStatic();

        ClassTest obj = new ClassTest();

        obj.showNonStatic();
    }
}
