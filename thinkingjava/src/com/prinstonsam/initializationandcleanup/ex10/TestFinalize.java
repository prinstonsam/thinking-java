/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.initializationandcleanup.ex10;

/**
 *
 * @author isamsonov
 */
public class TestFinalize {

    public void print()
    {
        System.out.println("print");
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            System.out.println("Error: checked out");
        } finally {
            super.finalize();
        }
     }

}
