/*
 * exercise 3 and exercise 4
 *
 *
 */
package com.prinstonsam.initializationandcleanup.ex03;

/**
 *
 * @author isamsonov
 */
public class ClassWithDefaultConstructor {

    public ClassWithDefaultConstructor() {
        System.out.println("Yep");
    }

    public ClassWithDefaultConstructor(String value) {
        System.out.println(value);
    }



}
