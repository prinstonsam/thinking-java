/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.initializationandcleanup.ex13;

/**
 *
 * @author isamsonov
 */
public class ClassForEx {

}

class Cup {
  int a;
  int b;
  {
      a = 100;
      b = 100;
  }
  Cup(int marker) {
      System.out.println("Cup(" + marker + ")");
  }

  void f(int marker) {
    System.out.println("f(" + marker + ")");
  }
}



class Cups {
  static Cup cup1;
  static Cup cup2;

  static {
    cup1 = new Cup(1);
    cup2 = new Cup(2);
  }



  Cups() {
    System.out.println("Cups()");
  }
}



class ExplicitStatic {
  public static void main(String[] args) {
    System.out.println("Inside main()");
    Cups.cup1.f(99);  // (1)

    Cup aaa = new Cup(100);

      System.out.println(aaa.a);

  }
}