/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.initializationandcleanup.ex15;

/**
 *
 * @author isamsonov
 */
public class TestInnerInitialization {
    String value;

    {
        value = "Inner initialization";
        System.out.println(value);
    }

    public TestInnerInitialization() {
        value = "Constructor initialization";

        System.out.println(value);
    }
}

class Main{
    public static void main(String[] args) {
        TestInnerInitialization obj = new TestInnerInitialization();
    }
}
