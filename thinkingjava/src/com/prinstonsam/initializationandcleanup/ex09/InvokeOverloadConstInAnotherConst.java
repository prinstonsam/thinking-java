/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.initializationandcleanup.ex09;

/**
 *
 * @author isamsonov
 */
public class InvokeOverloadConstInAnotherConst {

    private InvokeOverloadConstInAnotherConst(String value1, String value2) {
        System.out.println(value1);
        System.out.println(value2);

    }

    public InvokeOverloadConstInAnotherConst(String value) {
        this(value, "another value");
    }




}
