/*
 * exersice 5 and 6
 */
package com.prinstonsam.initializationandcleanup.ex05;

/**
 *
 * @author isamsonov
 */
public class Main {
    public static void main(String[] args) {
        Dog dog = new Dog();

        dog.Bark((short)1);
        dog.Bark(5.01);
        dog.Bark((short)1000000);

        dog.TwoBark((short)1, 1);
        dog.TwoBark(1, (short)1);
        dog.TwoBark(1, 1);
    }
}
