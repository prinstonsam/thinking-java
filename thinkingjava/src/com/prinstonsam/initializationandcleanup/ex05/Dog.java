/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.initializationandcleanup.ex05;

/**
 *
 * @author isamsonov
 */
public class Dog {

    public Dog() {
    }

    public void Bark(int value){
        System.out.println("barking");

    }
    public void Bark(double value){
        System.out.println("howling");

    }
    public void Bark(short value){
        System.out.println("whines");

    }

    public void TwoBark(short value1, int value2){
        System.out.println("whines barking");

    }

    public void TwoBark(int value2, short value1){
        System.out.println("barking whines");

    }

    public void TwoBark(int value2, int value1){
        System.out.println("barking barking");
    }
}
