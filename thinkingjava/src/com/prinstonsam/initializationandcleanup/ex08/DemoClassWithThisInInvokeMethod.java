/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prinstonsam.initializationandcleanup.ex08;

/**
 *
 * @author isamsonov
 */
public class DemoClassWithThisInInvokeMethod {

    public DemoClassWithThisInInvokeMethod() {
    }

    public void method1(){
        System.out.println("aaa");
    }

    public void method2(){
        method1();
        this.method1();
    }

}
